#pragma once

#include <VirtualRobot/RobotNodeSet.h>
#include <rbdl/rbdl.h>

#include <set>


namespace wholebodymotion::model
{

    /** @brief
     * Encapsulates dynamics simulations based on the RBDL library for the virtual robot.
     *
     */
    class DynamicsModel
    {
    public:
        /// Creates a Dynamics object given a RobotNodeSet.
        /// The rns has to be completely connected (avoid missing RobotNodes).
        /// The rns should end with a RobotNode that has a mass>0 specified, otherwise the last joint can not be added to the internal RBDL model
        ///
        DynamicsModel(VirtualRobot::RobotNodeSetPtr rns, VirtualRobot::RobotNodeSetPtr rnsBodies = VirtualRobot::RobotNodeSetPtr(), bool verbose = false);
        /// Calculates the Inverse Dynamics for given motion state defined by q, qdot and qddot
        Eigen::VectorXd getInverseDynamics(const Eigen::Ref<const Eigen::VectorXd> q, const Eigen::Ref<const Eigen::VectorXd> qdot, const Eigen::Ref<const Eigen::VectorXd> qddot);
        void getInverseDynamics(const Eigen::Ref<const Eigen::VectorXd> q, const Eigen::Ref<const Eigen::VectorXd> qdot, const Eigen::Ref<const Eigen::VectorXd> qddot, Eigen::VectorXd& tau);
        /// Calculates the joint space Gravity Matrix given a joint position vector q and Number of DOF
        Eigen::VectorXd getGravityMatrix(const Eigen::Ref<const Eigen::VectorXd> q);
        void getGravityMatrix(const Eigen::Ref<const Eigen::VectorXd> q, Eigen::VectorXd &tau);
        /// Calculates the coriolis matrix given position vector q, velocity vector qdot and Number of DOF
        Eigen::VectorXd getCoriolisMatrix(const Eigen::Ref<const Eigen::VectorXd> q, const Eigen::Ref<const Eigen::VectorXd> qdot);
        /// Calculates forward dynamics given position vector q velocity vector qdot and joint torques tau
        Eigen::VectorXd getForwardDynamics(const Eigen::Ref<const Eigen::VectorXd> q, const Eigen::Ref<const Eigen::VectorXd> qdot, Eigen::VectorXd tau);
        /**
         * @brief Calculates the joint space inertia matrix given a joint position vector q
         * @param q joint angles
         * @param updateKinematics if true, the forward kinematics are calculated based on q. Safer, but more costly.
         * @return InertiaMatrix with size nDoF x nDoF
         */
        Eigen::MatrixXd getInertiaMatrix(const Eigen::Ref<const Eigen::VectorXd> q, bool updateKinematics=true);
        void getInertiaMatrix(const Eigen::Ref<const Eigen::VectorXd> q, Eigen::MatrixXd& inertiaMatrix, bool updateKinematics=true);
        /// Sets the gravity vector of the dynamics system
        void setGravity(const Eigen::Vector3d &gravity);
        /// returns the number of Degrees of Freedom of the dynamics system
        int getnDoF();

        int getIdentifier(std::string name)
        {
            return identifierMap.at(name);
        }

        void print(bool printRBDL = false);

        RigidBodyDynamics::Body computeCombinedBody(const std::set<VirtualRobot::RobotNodePtr>& nodes, const VirtualRobot::RobotNodePtr &referenceNode) const;
        bool getVerbose() const;
        void setVerbose(bool value);

        std::shared_ptr<RigidBodyDynamics::Model> getModel() const;

//        [[deprecated("Not yet working correctly!")]]
//        void scale(float size, float mass = 1.0) {
//            scale(*model.get(), size, mass);
//        }

//        [[deprecated("Not yet working correctly!")]]
//        static void scale(RigidBodyDynamics::Model &model, float size, float mass = 1.0);

        static void convertRevoluteXYZ2Revolute(RigidBodyDynamics::Model &model);

    protected:
        VirtualRobot::RobotNodeSetPtr rns;
        VirtualRobot::RobotNodeSetPtr rnsBodies;
        std::shared_ptr<RigidBodyDynamics::Model> model;
        Eigen::Vector3d gravity;
        std::map<std::string,  int> identifierMap;
        bool verbose = false;
        Eigen::VectorXd zeroVec;
        std::map<std::string, std::string> parentMap;
        VirtualRobot::RobotNodePtr rootNode;

        std::set<VirtualRobot::RobotNodePtr> getChildrenWithMass(const VirtualRobot::RobotNodePtr& node, const VirtualRobot::RobotNodeSetPtr &nodeSet) const;
    private:
        void computeRobotNodeOrder(const VirtualRobot::RobotNodeSetPtr &rns);
        void toRBDL(std::shared_ptr<RigidBodyDynamics::Model> model, VirtualRobot::RobotNodePtr node, VirtualRobot::RobotNodeSetPtr nodeSet, VirtualRobot::RobotNodePtr parentNode = VirtualRobot::RobotNodePtr(),  int parentID = 0);
    };
}

