#include "DynamicsModel.h"

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/Nodes/RobotNode.h>
#include <VirtualRobot/Nodes/RobotNodeFactory.h>
#include <VirtualRobot/Nodes/RobotNodeRevolute.h>
#include <VirtualRobot/Nodes/RobotNodePrismatic.h>
#include <VirtualRobot/XML/RobotIO.h>
#include <VirtualRobot/Units.h>
#include <VirtualRobot/MathTools.h>

#include <Eigen/Dense>

#include <math.h>

#include <string>
#include <iostream>

#include <rbdl/rbdl_utils.h>

#define VERBOSE_OUT if(verbose) std::cout


namespace wholebodymotion::model
{
    void DynamicsModel::computeRobotNodeOrder(const VirtualRobot::RobotNodeSetPtr &rns) {
        auto robotNodes = rns->getAllRobotNodes();
        if (robotNodes.empty())
            throw std::runtime_error("RobotNodeSet is empty!");
        std::string rootName = robotNodes[0]->getName();
        std::set<std::string> prevNodes = { rootName };
        for (unsigned int i = 1; i < robotNodes.size(); i++)
        {
            std::string nodeName = robotNodes[i]->getName();
            if (!robotNodes[i]->getParent())
            {
                throw std::runtime_error("RobotNodeSet is not a kinematic chain! Parent of joint " + nodeName + " is empty at index " + std::to_string(i));
            }
            std::string parentName = robotNodes[i]->getParent()->getName();
            while (prevNodes.find(parentName) == prevNodes.end())
            {
                if (rns->hasRobotNode(parentName))
                    throw std::runtime_error("RobotNodeSet is not a kinematic chain! " + parentName +
                                       " was not yet defined but is parent of " + nodeName + " at index " + std::to_string(i));
                auto parentNode = rns->getRobot()->getRobotNode(parentName)->getParent();
                if (!parentNode)
                    break;
                else parentName = parentNode->getName();
            }
            parentMap[nodeName] = parentName;
            prevNodes.insert(robotNodes[i]->getName());
        }
    }

    DynamicsModel::DynamicsModel(VirtualRobot::RobotNodeSetPtr rns, VirtualRobot::RobotNodeSetPtr rnsBodies, bool verbose)
        : rns(rns), rnsBodies(rnsBodies), verbose(verbose)
    {
        if (!rns)
        {
            throw std::runtime_error("RobotNodeSetPtr for the joints is zero pointer");
        }
        VERBOSE_OUT << "joint values:\n" << rns->getJointValuesEigen() << std::endl;
        gravity = RigidBodyDynamics::Math::Vector3d(0, 0, -9.81);
        model = std::shared_ptr<RigidBodyDynamics::Model>(new RigidBodyDynamics::Model());

        model->gravity = gravity;

        computeRobotNodeOrder(rns);

        rootNode = rns->getNode(0)->getPhysics().massKg > 0 ? rns->getNode(0) : rns->getKinematicRoot();

        toRBDL(model, rootNode, rns);
        zeroVec = Eigen::VectorXd::Zero(model->dof_count);
    }

    Eigen::VectorXd DynamicsModel::getInverseDynamics(const Eigen::Ref<const Eigen::VectorXd> q, const Eigen::Ref<const Eigen::VectorXd> qdot, const Eigen::Ref<const Eigen::VectorXd> qddot)
    {
        Eigen::VectorXd tau = Eigen::VectorXd::Zero(DynamicsModel::model->dof_count);
        getInverseDynamics(q, qdot, qddot, tau);
        return tau;
    }

    void DynamicsModel::getInverseDynamics(const Eigen::Ref<const Eigen::VectorXd> q, const Eigen::Ref<const Eigen::VectorXd> qdot, const Eigen::Ref<const Eigen::VectorXd> qddot, Eigen::VectorXd& tau)
    {
        tau.setZero();
        VR_ASSERT(tau.rows() == q.rows());
        InverseDynamics(*model.get(), q, qdot, qddot, tau);
    }

    Eigen::VectorXd DynamicsModel::getForwardDynamics(const Eigen::Ref<const Eigen::VectorXd> q, const Eigen::Ref<const Eigen::VectorXd> qdot, Eigen::VectorXd tau)
    {
        Eigen::VectorXd qddot = Eigen::VectorXd::Zero(DynamicsModel::model->dof_count);
        ForwardDynamics(*model.get(), q, qdot, tau, qddot);
        return qddot;
    }


    Eigen::VectorXd DynamicsModel::getGravityMatrix(const Eigen::Ref<const Eigen::VectorXd> q)
    {
        Eigen::VectorXd tauGravity = Eigen::VectorXd::Zero(DynamicsModel::model->dof_count);
        getGravityMatrix(q, tauGravity);
        return tauGravity;
    }

    void DynamicsModel::getGravityMatrix(const Eigen::Ref<const Eigen::VectorXd> q, Eigen::VectorXd &tau)
    {
        VR_ASSERT(q.rows() == DynamicsModel::model->dof_count);
        InverseDynamics(*model.get(), q, zeroVec, zeroVec, tau);
    }

    Eigen::VectorXd DynamicsModel::getCoriolisMatrix(const Eigen::Ref<const Eigen::VectorXd> q, const Eigen::Ref<const Eigen::VectorXd> qdot)
    {
        //    Eigen::VectorXd qddot = Eigen::VectorXd::Zero(DynamicsModel::model->dof_count);
        Eigen::VectorXd tauGravity = getGravityMatrix(q);
        Eigen::VectorXd tau = Eigen::VectorXd::Zero(DynamicsModel::model->dof_count);

        Eigen::MatrixXd tauCoriolis = Eigen::VectorXd::Zero(DynamicsModel::model->dof_count);
        InverseDynamics(*model.get(), q, qdot, zeroVec, tau);
        tauCoriolis = tau - tauGravity;
        return tauCoriolis;
    }



    Eigen::MatrixXd DynamicsModel::getInertiaMatrix(const Eigen::Ref<const Eigen::VectorXd> q, bool updateKinematics)
    {
        Eigen::MatrixXd inertia = Eigen::MatrixXd::Zero(model->dof_count, model->dof_count);
        getInertiaMatrix(q, inertia, updateKinematics);
        return inertia;
    }

    void DynamicsModel::getInertiaMatrix(const Eigen::Ref<const Eigen::VectorXd> q, Eigen::MatrixXd& inertiaMatrix, bool updateKinematics)
    {
        CompositeRigidBodyAlgorithm(*model.get(), q, inertiaMatrix, updateKinematics);
    }

    void DynamicsModel::setGravity(const Eigen::Vector3d& gravity)
    {
        model->gravity = gravity;
    }

    int DynamicsModel::getnDoF()
    {
        return model->dof_count;
    }

    void DynamicsModel::print(bool printRBDL)
    {
        if (printRBDL)
        {
            std::string result = RigidBodyDynamics::Utils::GetModelHierarchy(*model.get());
            std::cout << "RBDL hierarchy of RNS:" << rns->getName() << std::endl;
            std::cout << result;
            result = RigidBodyDynamics::Utils::GetNamedBodyOriginsOverview(*model.get());
            std::cout << "RBDL origins of RNS:" << rns->getName() << std::endl;
            std::cout << result;
            result = RigidBodyDynamics::Utils::GetModelDOFOverview(*model.get());
            std::cout << "RBDL DoF of RNS:" << rns->getName() << std::endl;
            std::cout << result;
            std::cout << "\n";
        }
        for (const auto &nameID : model->mBodyNameMap)
        {
            if (nameID.second < model->mBodies.size())
            {
                std::cout << "+ Body " << nameID.first << "\n";
                std::cout << "  mass   : " << model->mBodies[nameID.second].mMass << "\n";
                std::cout << "  com    : " << model->mBodies[nameID.second].mCenterOfMass.transpose() << "\n";
                std::cout << "  inertia: " << model->mBodies[nameID.second].mInertia << "\n";
            }
        }
//        for (const RigidBodyDynamics::FixedBody &fbody : model->mFixedBodies)
//        {
//            fbody.mBaseTransform.r *= size;
//            fbody.mParentTransform.r *= size;
//            if (fbody.mMass > 0.00001)
//            {
//                fbody.mCenterOfMass *= size;
//                fbody.mMass *= mass;
//                fbody.mInertia *= std::pow(size, 2) * mass;
//            }

//            auto parent_body = model.mBodies[fbody.mMovableParent];
//            model.I[fbody.mMovableParent] =
//                    RigidBodyDynamics::Math::SpatialRigidBodyInertia::createFromMassComInertiaC (
//                        parent_body.mMass,
//                        parent_body.mCenterOfMass,
//                        parent_body.mInertia);
//        }
        std::cout << std::endl;
    }

    RigidBodyDynamics::Body DynamicsModel::computeCombinedBody(const std::set<VirtualRobot::RobotNodePtr>& nodes,
                                                               const VirtualRobot::RobotNodePtr& referenceNode) const
    {
        Eigen::Vector3d CoM = referenceNode->getCoMLocal().cast<double>() / 1000;
        RigidBodyDynamics::Math::Matrix3d inertia = referenceNode->getInertiaMatrix().cast<double>();

        auto mainBody = referenceNode->getMass() > 1e-6
                ? RigidBodyDynamics::Body(referenceNode->getMass(), CoM, inertia)
                : RigidBodyDynamics::Body();

        for (const auto &node : nodes)
        {
            Eigen::Vector3d CoM = node->getCoMLocal().cast<double>() / 1000;
            RigidBodyDynamics::Math::Matrix3d inertia = node->getInertiaMatrix().cast<double>();

            auto otherBody = RigidBodyDynamics::Body(node->getMass(), CoM, inertia);
            Eigen::Matrix4f transform = referenceNode->getTransformationTo(node);
            RigidBodyDynamics::Math::SpatialTransform rbdlTransform(transform.block<3, 3>(0, 0).cast<double>(),
                                                                    transform.block<3, 1>(0, 3).cast<double>() / 1000);
            mainBody.Join(rbdlTransform, otherBody);
        }
        // Make center of mass and inertia zero for better computation
        mainBody.mCenterOfMass = mainBody.mCenterOfMass.unaryExpr([](double value){ return (abs(value) < 1e-12) ? 0.0 : value; });
        mainBody.mInertia = mainBody.mInertia.unaryExpr([](double value){ return (abs(value) < 1e-12) ? 0.0 : value; });
        return mainBody;
    }

    bool DynamicsModel::getVerbose() const
    {
        return verbose;
    }

    void DynamicsModel::setVerbose(bool value)
    {
        verbose = value;
    }

    std::shared_ptr<RigidBodyDynamics::Model> DynamicsModel::getModel() const
    {
        return model;
    }

    /*
    void DynamicsModel::scale(RigidBodyDynamics::Model &model, float size, float mass) {
        unsigned int i = 0;
        for (RigidBodyDynamics::Body &body : model.mBodies)
        {
            if (body.mMass > 0.00001)
            {
                body.mCenterOfMass *= size;
                body.mMass *= mass;
                body.mInertia *= std::pow(size, 2) * mass;
                model.I[i] =
                        RigidBodyDynamics::Math::SpatialRigidBodyInertia::createFromMassComInertiaC (
                            body.mMass,
                            body.mCenterOfMass,
                            body.mInertia);
            }
            i++;
        }
        for (RigidBodyDynamics::FixedBody &fbody : model.mFixedBodies)
        {
            fbody.mBaseTransform.r *= size;
            fbody.mParentTransform.r *= size;
            if (fbody.mMass > 0.00001)
            {
                fbody.mCenterOfMass *= size;
                fbody.mMass *= mass;
                fbody.mInertia *= std::pow(size, 2) * mass;
            }

            auto parent_body = model.mBodies[fbody.mMovableParent];
            model.I[fbody.mMovableParent] =
                    RigidBodyDynamics::Math::SpatialRigidBodyInertia::createFromMassComInertiaC (
                        parent_body.mMass,
                        parent_body.mCenterOfMass,
                        parent_body.mInertia);
        }

    }
    */

    void DynamicsModel::convertRevoluteXYZ2Revolute(RigidBodyDynamics::Model &model) {
        for (auto &joint : model.mJoints)
        {
            switch (joint.mJointType) {
            case RigidBodyDynamics::JointType::JointTypeRevoluteX:
            case RigidBodyDynamics::JointType::JointTypeRevoluteY:
            case RigidBodyDynamics::JointType::JointTypeRevoluteZ:
                joint.mJointType = RigidBodyDynamics::JointType::JointTypeRevolute;
            default:
                break;
            }
        }
    }

    std::set<VirtualRobot::RobotNodePtr> DynamicsModel::getChildrenWithMass(const VirtualRobot::RobotNodePtr& node,
                                                                            const VirtualRobot::RobotNodeSetPtr& nodeSet) const
    {
        std::set<VirtualRobot::RobotNodePtr> result;
        for (auto& obj : node->getChildren())
        {
            auto node = std::dynamic_pointer_cast<VirtualRobot::RobotNode>(obj);
            if (!node || nodeSet->hasRobotNode(node))
                continue;

            if (obj->getMass() > 1e-6 && (!rnsBodies || rnsBodies->hasRobotNode(node)))
            {
                result.insert(node);
            }

            auto tmp = getChildrenWithMass(node, nodeSet);
            result.insert(tmp.begin(), tmp.end());
        }
        return result;
    }

    void DynamicsModel::toRBDL(std::shared_ptr<RigidBodyDynamics::Model> model, VirtualRobot::RobotNodePtr node,
                               VirtualRobot::RobotNodeSetPtr nodeSet, VirtualRobot::RobotNodePtr parentNode, int parentID)
    {
        VirtualRobot::RobotNodePtr physicsFromChild;
        int nodeID = parentID;
        // need to define body, joint and spatial transform
        // body first
        auto relevantChildNodes = getChildrenWithMass(node, nodeSet);
        auto combinedBody = computeCombinedBody(relevantChildNodes, node);


        RigidBodyDynamics::Body body = combinedBody; //Body(mass, com, inertia);


        // spatial transform next
        Eigen::Matrix4d trafo = Eigen::Matrix4d::Identity();

        if (parentNode)
        {
            trafo = node->getTransformationFrom(parentNode).cast<double>();
        }
        else if (!parentNode)
        {
            trafo = node->getGlobalPose().cast<double>();//node->getTransformationFrom(rns->getKinematicRoot()).cast<double>();
        }

        RigidBodyDynamics::Math::Matrix3d spatial_rotation = trafo.block(0, 0, 3, 3);
        RigidBodyDynamics::Math::Vector3d spatial_translation = trafo.col(3).head(3) / 1000;

        //VERBOSE_OUT << "****** spatial_translation: " << spatial_translation.transpose() << std::endl;

        RigidBodyDynamics::Math::SpatialTransform spatial_transform = RigidBodyDynamics::Math::SpatialTransform(spatial_rotation.transpose(), spatial_translation);

        // last, joint
        RigidBodyDynamics::Joint joint = RigidBodyDynamics::Joint(RigidBodyDynamics::JointTypeFixed);

        if (node->isRotationalJoint())
        {
            RigidBodyDynamics::JointType joint_type = RigidBodyDynamics::JointTypeRevolute;
            std::shared_ptr<VirtualRobot::RobotNodeRevolute> rev = std::dynamic_pointer_cast<VirtualRobot::RobotNodeRevolute>(node);
            RigidBodyDynamics::Math::Vector3d joint_axis = rev->getJointRotationAxisInJointCoordSystem().cast<double>();

            joint = RigidBodyDynamics::Joint(joint_type, joint_axis);
        }
        else if (node->isTranslationalJoint())
        {
            RigidBodyDynamics::JointType joint_type = RigidBodyDynamics::JointTypePrismatic;
            std::shared_ptr<VirtualRobot::RobotNodePrismatic> prism = std::dynamic_pointer_cast<VirtualRobot::RobotNodePrismatic>(node);
            RigidBodyDynamics::Math::Vector3d joint_axis = prism->getJointTranslationDirectionJointCoordSystem().cast<double>();

            joint = RigidBodyDynamics::Joint(joint_type, joint_axis);
        }

        if (true || joint.mJointType != RigidBodyDynamics::JointTypeFixed)
        {
            nodeID = model->AddBody(parentID, spatial_transform, joint, body, node->getName());
            this->identifierMap[node->getName()] = nodeID;
            Eigen::VectorXd QDDot = Eigen::VectorXd::Zero(identifierMap.size());

            VERBOSE_OUT << "+ Adding Body: " << node->getName() << "\n";
            for (const auto &node : relevantChildNodes)
            {
                VERBOSE_OUT << "  (additional child '" << node->getName() << "' - " << node->getMass() << " kg)" << std::endl;
            }
            VERBOSE_OUT << "  parent_id  : " << parentID << "\n";
            VERBOSE_OUT << "  joint frame: X.E = \n" << spatial_transform.E << "\n";
            VERBOSE_OUT << "X.R = " << spatial_transform.r.transpose() << "\n";
            VERBOSE_OUT << "  joint dofs : 1\n";
            if (joint.mJointAxes) VERBOSE_OUT << "    0: " << joint.mJointAxes->transpose() << "\n";
            VERBOSE_OUT << "  body inertia:\n" << body.mInertia << "\n";
            VERBOSE_OUT << "  body mass   : " << body.mMass <<  "\n";
            VERBOSE_OUT << "  com         :" << body.mCenterOfMass.transpose() << "\n";
            VERBOSE_OUT << "  body name   : " << node->getName() << std::endl;;

            Eigen::Vector3d bodyPosition = RigidBodyDynamics::CalcBodyToBaseCoordinates(*model, Eigen::VectorXd::Zero(identifierMap.size()),
                                                                                        nodeID, Eigen::Vector3d::Zero(), true);
            Eigen::Vector3f positionInVirtualRobot = rns->getKinematicRoot() != rootNode ? node->getGlobalPosition() : rootNode->getTransformationTo(node).block(0, 3, 3, 1);
            auto diff = (positionInVirtualRobot - bodyPosition.cast<float>() * 1000).norm();
            if (false && diff > 0.01)
            {
                VR_ERROR << "forward kinematics between virtual robot and rbdl differ: " << diff << " mm";
                throw std::runtime_error("forward kinematics between virtual robot and rbdl differ!");
            }
            //        VERBOSE_OUT << "** position:\n" << bodyPosition << "\nposition in virtual robot:\n" << positionInVirtualRobot << endl << std::endl;


        }

        if (!rns->hasRobotNode(node) && rns->getKinematicRoot() == node && rns->size() > 0) {
            toRBDL(model, rns->getNode(0), rns, node, nodeID);
            return;
        }

        for (size_t i = 0; i < nodeSet->getSize(); ++i)
        {
            if (nodeSet->getNode(i) == node && i + 1 < nodeSet->getSize())
            {
                auto nextNode = nodeSet->getNode(i + 1);
                auto parentNextNode = rns->getNode(parentMap[nextNode->getName()]);
                toRBDL(model, nextNode, rns, parentNextNode, identifierMap[parentNextNode->getName()]);
            }
        }
    }
}
