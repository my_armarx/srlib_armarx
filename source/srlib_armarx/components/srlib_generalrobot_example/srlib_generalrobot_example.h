/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    srLibArmarX::ArmarXObjects::srLibGeneralRobotExample
 * @author     Byungchul An ( byungchul dot an at gmail dot com )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


// #include <mutex>

#include <ArmarXCore/core/Component.h>
#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotStateComponentPlugin.h>
#include <VirtualRobot/VirtualRobot.h>
#include <srlib_armarx/libraries/virtualrobot_to_srlib_generalrobot/virtualrobot_to_srlib_generalrobot.h>

// #include <ArmarXCore/libraries/ArmarXCoreComponentPlugins/DebugObserverComponentPlugin.h>

// #include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>

// #include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

#include "./dynamics_model/DynamicsModel.h"


namespace armarx
{

    /**
     * @defgroup Component-srLibGeneralRobotExample srLibGeneralRobotExample
     * @ingroup srLibArmarX-Components
     * A description of the component srLibGeneralRobotExample.
     *
     * @class srLibGeneralRobotExample
     * @ingroup Component-srLibGeneralRobotExample
     * @brief Brief description of class srLibGeneralRobotExample.
     *
     * Detailed description of class srLibGeneralRobotExample.
     */
    class srLibGeneralRobotExample :
        virtual public armarx::Component
        , public RobotStateComponentPluginUser
        // , virtual public armarx::DebugObserverComponentPluginUser
        // , virtual public armarx::LightweightRemoteGuiComponentPluginUser
        // , virtual public armarx::ArVizComponentPluginUser
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;
        static std::string GetDefaultName();


        RobotUnitInterfacePrx robotUnit;
        VirtualRobot::RobotPtr robot;
        VirtualRobotToSRLibGeneralRobot* srGRobot = nullptr;

    protected:

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;


        /* (Requires armarx::LightweightRemoteGuiComponentPluginUser.)
        /// This function should be called once in onConnect() or when you
        /// need to re-create the Remote GUI tab.
        void createRemoteGuiTab();

        /// After calling `RemoteGui_startRunningTask`, this function is
        /// called periodically in a separate thread. If you update variables,
        /// make sure to synchronize access to them.
        void RemoteGui_update() override;
        */


    private:

        // Private methods go here.

        // Forward declare `Properties` if you used it before its defined.
        // struct Properties;

        /* (Requires the armarx::ArVizComponentPluginUser.)
        /// Draw some boxes in ArViz.
        void drawBoxes(const Properties& p, viz::Client& arviz);
        */


    private:

        // Private member variables go here.
        VirtualRobot::RobotPtr cloneModel;
        std::shared_ptr<wholebodymotion::model::DynamicsModel> dynamics_model;


        /// Properties shown in the Scenario GUI.
        struct Properties
        {
            std::string boxLayerName;
            int numBoxes = 10;
        };
        Properties properties;
        /* Use a mutex if you access variables from different threads
         * (e.g. ice functions and RemoteGui_update()).
        std::mutex propertiesMutex;
        */


        /* (Requires the armarx::LightweightRemoteGuiComponentPluginUser.)
        /// Tab shown in the Remote GUI.
        struct RemoteGuiTab : armarx::RemoteGui::Client::Tab
        {
            armarx::RemoteGui::Client::LineEdit boxLayerName;
            armarx::RemoteGui::Client::IntSpinBox numBoxes;

            armarx::RemoteGui::Client::Button drawBoxes;
        };
        RemoteGuiTab tab;
        */


        /* (Requires the armarx::ArVizComponentPluginUser.)
         * When used from different threads, an ArViz client needs to be synchronized.
        /// Protects the arviz client inherited from the ArViz plugin.
        std::mutex arvizMutex;
        */


    };
}
