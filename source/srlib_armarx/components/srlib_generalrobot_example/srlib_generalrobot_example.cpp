/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    srLibArmarX::ArmarXObjects::srLibGeneralRobotExample
 * @author     Byungchul An ( byungchul dot an at gmail dot com )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "srlib_generalrobot_example.h"

#include <Eigen/Core>

#include <ArmarXCore/core/logging/Logging.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/XML/RobotIO.h>
#include <VirtualRobot/CollisionDetection/CollisionModel.h>
// Include headers you only need in function definitions in the .cpp.

// #include <Eigen/Core>

// #include <SimoxUtility/color/Color.h>
#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>
#include <srlib_armarx/libraries/srLib/srLib_armarx_converter.h>

namespace armarx
{

    armarx::PropertyDefinitionsPtr srLibGeneralRobotExample::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def = new ComponentPropertyDefinitions(getConfigIdentifier());

        // Publish to a topic (passing the TopicListenerPrx).
        // def->topic(myTopicListener);

        // Subscribe to a topic (passing the topic name).
        // def->topic<PlatformUnitListener>("MyTopic");

        // Use (and depend on) another component (passing the ComponentInterfacePrx).
        // def->component(myComponentProxy)


        // Add a required property.
        //        def->required(properties.boxLayerName, "p.box.LayerName", "Name of the box layer in ArViz.");

        // Add an optionalproperty.
        //        def->optional(properties.numBoxes, "p.box.Number", "Number of boxes to draw in ArViz.");


        // Armar6-RightArmRobotUnit: ROBDEKON_ObjectCleaningSim
        // Armar6Unit: Armar6SimulationUsingRobotUnit
        def->defineOptionalProperty<std::string>("RobotUnitName", "Armar6-RightArmRobotUnit", "Robot unit (duh)");


        return def;
    }


    void srLibGeneralRobotExample::onInitComponent()
    {
        // Topics and properties defined above are automagically registered.

        // Keep debug observer data until calling `sendDebugObserverBatch()`.
        // (Requies the armarx::DebugObserverComponentPluginUser.)
        // setDebugObserverBatchModeEnabled(true);
    }


    void srLibGeneralRobotExample::onConnectComponent()
    {
        // Do things after connecting to topics and components.
        getProxyFromProperty(robotUnit, "RobotUnitName");

        //        robot = addRobot("robot", VirtualRobot::RobotIO::eStructure);
        robot = addRobot("robot", VirtualRobot::RobotIO::eFull);
        std::string rnsName("RightArm");
        VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSet(rnsName);

        ARMARX_INFO << "joint val: " << rns->getJointValuesEigen().transpose();

        Eigen::VectorXf randJointVal(rns->getSize());
        //        randJointVal.setRandom();
        randJointVal.setZero();
        rns->setJointValues(randJointVal);

        ARMARX_INFO << "joint val: " << rns->getJointValuesEigen().transpose();

        Eigen::Matrix4f T = Eigen::Matrix4f::Identity();
        T(0, 0) = 0; T(0, 1) = 1;
        T(1, 0) = 1; T(1, 1) = 0;
        Eigen::Matrix6f AdT = srLib::Ad(T);
        ARMARX_INFO << "AD: ";
        ARMARX_INFO << AdT;
        ARMARX_INFO << "AD TRANSPOSE: ";
        ARMARX_INFO << srLib::Ad(T).transpose();

        Eigen::Matrix4f T2 = Eigen::Matrix4f::Identity();
        T2.block<3, 1>(0, 3) << 1, 2, 3;
        T2.block<3, 1>(0, 3) = T2.block<3, 1>(0, 3) * 0.001f;
        Eigen::Matrix4f T3 = T2 * T;
        Eigen::Matrix3f R3 = T3.block<3, 3>(0, 0);
        ARMARX_INFO << T2;
        ARMARX_INFO << "LOG:";
        ARMARX_INFO << srLib::LogSE3(T2 * T);
        ARMARX_INFO << "RPY";
        ARMARX_INFO << VirtualRobot::MathTools::eigen3f2rpy(R3);
        ARMARX_INFO << srLib::Ad(T2 * T);

        Eigen::Matrix4f T4;
        VirtualRobot::MathTools::rpy2eigen4f(0.1, 0.2, 0.3, T4);
        ARMARX_INFO << "RPY";
        ARMARX_INFO << VirtualRobot::MathTools::eigen4f2rpy(T4);
        ARMARX_INFO << "LOG";
        ARMARX_INFO << srLib::LogSE3(T4);

//        Eigen::Vector6f s;
//        s << 0, 0, 0, 1, 0, 0;
//        Eigen::Matrix6f S = s.asDiagonal();
//        ARMARX_INFO << S;
//        Eigen::Matrix6f I6;
//        I6.setIdentity();
//        ARMARX_INFO << I6;
//        ARMARX_INFO << I6 - S;



        srGRobot = new VirtualRobotToSRLibGeneralRobot(robot, rnsName);

        bool inRange = srGRobot->forwardKinematics(randJointVal);
        ARMARX_INFO << "IN RANGE: " << inRange << ", joint values: " << randJointVal.transpose();
        ARMARX_INFO << "FORWARD KIN SRLIB: " << srGRobot->getTCPFrame();
        ARMARX_INFO << "FORWARD KIN VR: " << rns->getTCP()->getPoseInRootFrame();
        ARMARX_INFO << "GLOBAL DIFF: " <<  srGRobot->getTCPFrameInGlobal().inverse() * srGRobot->convertToM(rns->getTCP()->getGlobalPose());

        randJointVal.setRandom();
        rns->setJointValues(randJointVal);
        inRange = srGRobot->forwardKinematics(randJointVal);
        ARMARX_INFO << "IN RANGE: " << inRange << ", joint values: " << randJointVal.transpose();
        ARMARX_INFO << "FORWARD KIN SRLIB: " << srGRobot->getTCPFrame();
        ARMARX_INFO << "FORWARD KIN VR: " << rns->getTCP()->getPoseInRootFrame();
        ARMARX_INFO << "GLOBAL DIFF: " << srGRobot->getTCPFrameInGlobal().inverse() * srGRobot->convertToM(rns->getTCP()->getGlobalPose());

        srGRobot->forwardKinematics();
        ARMARX_INFO << "FORWARD KIN SRLIB: " << srGRobot->getTCPFrame();

        srGRobot->exportRobotParameters();


        std::string rnsLeftName("LeftArm");
        VirtualRobotToSRLibGeneralRobot* srGRobotLeft = new VirtualRobotToSRLibGeneralRobot(robot, rnsLeftName);
        srGRobotLeft->exportRobotParameters();

        cloneModel = rns->getRobot()->clone(rns->getRobot()->getName(), VirtualRobot::CollisionCheckerPtr(), -1.0f);
        dynamics_model = std::shared_ptr<wholebodymotion::model::DynamicsModel>(new wholebodymotion::model::DynamicsModel(rns, cloneModel->getRobotNodeSet("All"), false));
        Eigen::MatrixXd JointMass = dynamics_model->getInertiaMatrix(randJointVal.cast<double>(), false);

        ARMARX_INFO << "joint angles: " << randJointVal;
        ARMARX_INFO << "INERTIA M: " << JointMass;

        /* (Requies the armarx::DebugObserverComponentPluginUser.)
        // Use the debug observer to log data over time.
        // The data can be viewed in the ObserverView and the LivePlotter.
        // (Before starting any threads, we don't need to lock mutexes.)
        {
            setDebugObserverDatafield("numBoxes", properties.numBoxes);
            setDebugObserverDatafield("boxLayerName", properties.boxLayerName);
            sendDebugObserverBatch();
        }
        */

        /* (Requires the armarx::ArVizComponentPluginUser.)
        // Draw boxes in ArViz.
        // (Before starting any threads, we don't need to lock mutexes.)
        drawBoxes(properties, arviz);
        */

        /* (Requires the armarx::LightweightRemoteGuiComponentPluginUser.)
        // Setup the remote GUI.
        {
            createRemoteGuiTab();
            RemoteGui_startRunningTask();
        }
        */
    }


    void srLibGeneralRobotExample::onDisconnectComponent()
    {
        srGRobot->~VirtualRobotToSRLibGeneralRobot();
    }


    void srLibGeneralRobotExample::onExitComponent()
    {

    }


    std::string srLibGeneralRobotExample::getDefaultName() const
    {
        return "srLibGeneralRobotExample";
    }

    string srLibGeneralRobotExample::GetDefaultName()
    {
        return "srLibGeneralRobotExample";
    }


    /* (Requires the armarx::LightweightRemoteGuiComponentPluginUser.)
    void srLibGeneralRobotExample::createRemoteGuiTab()
    {
        using namespace armarx::RemoteGui::Client;

        // Setup the widgets.

        tab.boxLayerName.setValue(properties.boxLayerName);

        tab.numBoxes.setValue(properties.numBoxes);
        tab.numBoxes.setRange(0, 100);

        tab.drawBoxes.setLabel("Draw Boxes");

        // Setup the layout.

        GridLayout grid;
        int row = 0;
        {
            grid.add(Label("Box Layer"), {row, 0}).add(tab.boxLayerName, {row, 1});
            ++row;

            grid.add(Label("Num Boxes"), {row, 0}).add(tab.numBoxes, {row, 1});
            ++row;

            grid.add(tab.drawBoxes, {row, 0}, {2, 1});
            ++row;
        }

        VBoxLayout root = {grid, VSpacer()};
        RemoteGui_createTab(getName(), root, &tab);
    }


    void srLibGeneralRobotExample::RemoteGui_update()
    {
        if (tab.boxLayerName.hasValueChanged() || tab.numBoxes.hasValueChanged())
        {
            std::scoped_lock lock(propertiesMutex);
            properties.boxLayerName = tab.boxLayerName.getValue();
            properties.numBoxes = tab.numBoxes.getValue();

            {
                setDebugObserverDatafield("numBoxes", properties.numBoxes);
                setDebugObserverDatafield("boxLayerName", properties.boxLayerName);
                sendDebugObserverBatch();
            }
        }
        if (tab.drawBoxes.wasClicked())
        {
            // Lock shared variables in methods running in seperate threads
            // and pass them to functions. This way, the called functions do
            // not need to think about locking.
            std::scoped_lock lock(propertiesMutex, arvizMutex);
            drawBoxes(properties, arviz);
        }
    }
    */

    /* (Requires the armarx::ArVizComponentPluginUser.)
    void srLibGeneralRobotExample::drawBoxes(const srLibGeneralRobotExample::Properties& p, viz::Client& arviz)
    {
        // Draw something in ArViz (requires the armarx::ArVizComponentPluginUser.
        // See the ArVizExample in RobotAPI for more examples.

        viz::Layer layer = arviz.layer(p.boxLayerName);
        for (int i = 0; i < p.numBoxes; ++i)
        {
            layer.add(viz::Box("box_" + std::to_string(i))
                      .position(Eigen::Vector3f(i * 100, 0, 0))
                      .size(20).color(simox::Color::blue()));
        }
        arviz.commit(layer);
    }
    */

    ARMARX_REGISTER_COMPONENT_EXECUTABLE(srLibGeneralRobotExample, srLibGeneralRobotExample::GetDefaultName());
}
