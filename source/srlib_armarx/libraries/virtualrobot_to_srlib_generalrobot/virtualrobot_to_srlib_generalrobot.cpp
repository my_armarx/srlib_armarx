/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    srLibArmarX::ArmarXObjects::VirtualRobotToSRLibGeneralRobot
 * @author     Byungchul An ( byungchul dot an at gmail dot com )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "virtualrobot_to_srlib_generalrobot.h"

#include <vector>
#include <string>

#include <ArmarXCore/core/logging/Logging.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/Nodes/RobotNodeRevolute.h>
#include <VirtualRobot/Nodes/RobotNodePrismatic.h>
#include <VirtualRobot/math/Helpers.h>
#include <SimoxUtility/json.h>
#include <VirtualRobot/CollisionDetection/CollisionModel.h>
#include <VirtualRobot/BoundingBox.h>


namespace armarx
{

VirtualRobotToSRLibGeneralRobot::VirtualRobotToSRLibGeneralRobot(const VirtualRobot::RobotPtr& virtualRobot, const string &_robotNodeSetName)
{
    rns = virtualRobot->getRobotNodeSet(_robotNodeSetName);
    robotNodeSetName = _robotNodeSetName;
    unsigned int numNodes = rns->getSize();
    Eigen::VectorXf initJointVal(numNodes);
    Eigen::VectorXf jointVal(numNodes);
    jointVal = rns->getJointValuesEigen();
    initJointVal.setZero();

    rns->setJointValues(initJointVal);
    baseFrame = EigenToSE3(convertToM(virtualRobot->getRootNode()->getGlobalPose()));
    // TODO: set general robot
    convertRobot(virtualRobot);
    thetaPtr = gRobot->getThetaPointer();


    // return to the original joint values --> unnecessary
}

VirtualRobotToSRLibGeneralRobot::~VirtualRobotToSRLibGeneralRobot()
{
    gRobot->~generalRobot();
    delete gRobot;
    delete thetaPtr;
}

SE3 VirtualRobotToSRLibGeneralRobot::EigenToSE3(const Eigen::Matrix4f &_in)
{
    SE3 temp = SE3(_in(0,0), _in(1, 0), _in(2,0), _in(0,1), _in(1, 1), _in(2,1),
                   _in(0,2), _in(1, 2), _in(2,2), _in(0,3), _in(1, 3), _in(2,3));
    return temp;
}

Eigen::Matrix4f VirtualRobotToSRLibGeneralRobot::SE3ToEigen(const SE3 &_in)
{
    Eigen::Matrix4f ret;
    ret.col(0) << _in[0], _in[1], _in[2], 0.0f;
    ret.col(1) << _in[3], _in[4], _in[5], 0.0f;
    ret.col(2) << _in[6], _in[7], _in[8], 0.0f;
    ret.col(3) << _in[9], _in[10], _in[11], 1.0f;

    return ret;
}

Eigen::Matrix4f VirtualRobotToSRLibGeneralRobot::convertToM(const Eigen::Matrix4f &_in)
{
    Eigen::Matrix4f rtn = Eigen::Matrix4f::Identity();
    rtn.block<3, 3>(0, 0) = _in.block<3, 3>(0, 0);
    rtn.block<3, 1>(0, 3) = _in.block<3, 1>(0, 3) * 0.001f;
    return rtn;
}

Eigen::MatrixXf VirtualRobotToSRLibGeneralRobot::convertInertia(const Inertia &M)
{
    float Marray[36];
    M.ToArray(Marray);

    std::vector<float> Mv(std::begin(Marray), std::end(Marray));
    //    ARMARX_INFO << "CONVERT INERTIA TEST: ";
    //    ARMARX_INFO << Marray[0] << ", " << Marray[1] << ", " << Marray[2] << ", " << Marray[3] << ", " << Marray[4] << ", " << Marray[5];

    Eigen::MatrixXf G = Eigen::Map<Eigen::Matrix<float, 6, 6> >(Mv.data());

    return G;
}

bool VirtualRobotToSRLibGeneralRobot::forwardKinematics()
{
    return gRobot->forwardKinematics();
}

bool VirtualRobotToSRLibGeneralRobot::forwardKinematics(const Eigen::VectorXf &_theta)
{
    /* return true: if _theta is in joint range
     * return false: if _theta exceeds joint limits
     */
    Eigen::VectorXd theta = _theta.cast<double>();
    bool inJointRange = gRobot->forwardKinematics(&theta);

    return inJointRange;
}

Eigen::VectorXd VirtualRobotToSRLibGeneralRobot::inverseDynamics(const Eigen::VectorXf &_theta, const Eigen::VectorXf &_thetaDot, const Eigen::VectorXf &_thetaDDot)
{
    Eigen::VectorXd tau;

    Eigen::VectorXd theta = _theta.cast<double>();
    Eigen::VectorXd thetaDot = _thetaDot.cast<double>();
    Eigen::VectorXd thetaDDot = _thetaDDot.cast<double>();
    tau = gRobot->inverseDynamics(&thetaDDot, &theta, &thetaDot);

    return tau;
}

Eigen::MatrixXd VirtualRobotToSRLibGeneralRobot::getMassMatrix(const Eigen::VectorXf &theta)
{
    std::lock_guard<std::mutex> lock(thetaptr_mutex);
    Eigen::VectorXd tempTheta = *thetaPtr;
    *thetaPtr = theta.cast<double>();
    mass = gRobot->getMassMatrix();

    *thetaPtr = tempTheta;

    return mass;
}

Eigen::VectorXd VirtualRobotToSRLibGeneralRobot::getCoriolisGravity(const Eigen::VectorXf &theta, const Eigen::VectorXf &thetaDot)
{
    Eigen::VectorXf thetaDDot = theta;
    thetaDDot.setZero();

    coriolis_gravity = inverseDynamics(thetaDDot, theta, thetaDot);

    return coriolis_gravity;
}

Eigen::Matrix4f VirtualRobotToSRLibGeneralRobot::getTCPFrame()
{
//    SE3 toolFrame = gRobot->getToolFrame();
    return SE3ToEigen(gRobot->getToolFrame());
}

Eigen::Matrix4f VirtualRobotToSRLibGeneralRobot::getTCPFrameInGlobal()
{
    return SE3ToEigen(this->baseFrame * gRobot->getToolFrame());
}

Eigen::Matrix4f VirtualRobotToSRLibGeneralRobot::getLinkFrame(unsigned int index)
{
    return SE3ToEigen(gRobot->getLinkFrame(index));
}

Eigen::MatrixXf VirtualRobotToSRLibGeneralRobot::getBodyJacobian()
{
    return gRobot->getToolJacobian().cast<float>();
}

Eigen::MatrixXf VirtualRobotToSRLibGeneralRobot::getBodyJacobianInRoot()
{
    Eigen::MatrixXd ret;
    Eigen::Matrix4f baseOrientation = Eigen::Matrix4f::Identity();
    math::Helpers::Orientation(baseOrientation) = math::Helpers::Orientation(SE3ToEigen(gRobot->getToolFrame()));

    ret = Ad(EigenToSE3(baseOrientation)) * gRobot->getToolJacobian();
    return ret.cast<float>();
}

Eigen::MatrixXf VirtualRobotToSRLibGeneralRobot::getBodyJacobianInGlobal()
{
    Eigen::MatrixXd ret;
    Eigen::Matrix4f baseOrientation = Eigen::Matrix4f::Identity();
    math::Helpers::Orientation(baseOrientation) = math::Helpers::Orientation(SE3ToEigen(this->baseFrame * gRobot->getToolFrame()));

    ret = Ad(EigenToSE3(baseOrientation)) * gRobot->getToolJacobian();
    return ret.cast<float>();
}

Eigen::MatrixXf VirtualRobotToSRLibGeneralRobot::getSpaceJacobian()
{
    return gRobot->getSpaceJacobian().cast<float>();
}

Eigen::MatrixXf VirtualRobotToSRLibGeneralRobot::getSpaceJacobianInGlobal()
{
    return (Ad(this->baseFrame) * gRobot->getSpaceJacobian()).cast<float>();
}

void VirtualRobotToSRLibGeneralRobot::exportRobotParameters()
{
    std::string robotName = rns->getRobot()->getName();
    std::string relativeFilename = robotName + "_" + robotNodeSetName + "_parameters2.txt" ;

    nlohmann::json myJson; // = nlohmann::json::array();

    std::vector<se3> screws_se3 = gRobot->getScrews();
    std::vector<Eigen::VectorXd> screws;
    int ind = 0;
    for (auto sc : screws_se3)
    {
        screws.push_back(se3ToVectorXd(sc));
        nlohmann::json j =
        {
            { "id", ind},
            { "value", screws.back()}
        };
        myJson["screw"].push_back(j);
        ++ind;
    }

    std::vector<bool> isJoint = gRobot->getConnectionStatus();
    ind = 0;
    for (auto isJ : isJoint)
    {
        nlohmann::json j =
        {
            { "id", ind},
            { "value", static_cast<unsigned int>(isJ)}
        };
        myJson["isJoint"].push_back(j);
        ++ind;
    }

    std::vector<SE3> linkTransform = gRobot->getLinkTransform();
    ind = 0;
    for (const auto& linkM : linkTransform)
    {
        Eigen::Matrix4f tempM = SE3ToEigen(linkM);
        nlohmann::json j =
        {
            { "id", ind},
            { "row 1", tempM.row(0)},
            { "row 2", tempM.row(1)},
            { "row 3", tempM.row(2)}
        };
        myJson["link transform"].push_back(j);
        ++ind;
    }

    std::vector<Inertia> linkInertia = gRobot->getInertias();
    ind = 0;
    for (auto linkI : linkInertia)
    {
        nlohmann::json j =
        {
            {"id", ind},
            {"ixx", linkI[0]},
            {"iyy", linkI[1]},
            {"izz", linkI[2]},
            {"ixy", linkI[3]},
            {"ixz", linkI[4]},
            {"iyz", linkI[5]},
            {"r0", linkI[6]},
            {"r1", linkI[7]},
            {"r2", linkI[8]},
            {"mass", linkI[9]}
        };
        myJson["link inertia"].push_back(j);
        ++ind;
    }

    Eigen::Matrix4f baseM = SE3ToEigen(gRobot->getBaseFrame());
    Eigen::Matrix4f tcpM = SE3ToEigen(gRobot->getTCPTransform());
    Eigen::Matrix4f linkFTM = SE3ToEigen(gRobot->getFTSensorFrame());
    Eigen::Matrix4f tcpFTM = SE3ToEigen(M_tcp_ft);
    {
        nlohmann::json baseJ =
        {
            {"id", "base"},
            {"row 1", baseM.row(0)},
            {"row 2", baseM.row(1)},
            {"row 3", baseM.row(2)}
        };
        myJson["frames"].push_back(baseJ);

        nlohmann::json tcpJ =
        {
            {"id", "tcp"},
            {"row 1", tcpM.row(0)},
            {"row 2", tcpM.row(1)},
            {"row 3", tcpM.row(2)}
        };
        myJson["frames"].push_back(tcpJ);

        nlohmann::json ftJ =
        {
            {"id", "link8_FT"},
            {"row 1", linkFTM.row(0)},
            {"row 2", linkFTM.row(1)},
            {"row 3", linkFTM.row(2)}
        };
        myJson["frames"].push_back(ftJ);

        nlohmann::json tcpFTJ =
        {
            {"id", "FTSensor"},
            {"row 1", tcpFTM.row(0)},
            {"row 2", tcpFTM.row(1)},
            {"row 3", tcpFTM.row(2)}
        };
        myJson["frames"].push_back(tcpFTJ);
    }

    Eigen::VectorXd upperLimits = gRobot->getUpperLimits();
    Eigen::VectorXd lowerLimits = gRobot->getLowerLimits();
    {
        nlohmann::json ju =
        {
            {"id", "upper limits"},
            {"value", upperLimits}
        };
        myJson["joint limits"].push_back(ju);

        nlohmann::json jl =
        {
            {"id", "lower limits"},
            {"value", lowerLimits}
        };
        myJson["joint limits"].push_back(jl);
    }

    // Bounding box in local coordinate
    ind = 0;
    for (auto linkBB : linkBoundingBox)
    {
        nlohmann::json j =
        {
            { "id", ind},
            { "max", linkBB.maxValues},
            { "min", linkBB.minValues},
            { "com", linkBB.com}
        };
        myJson["link AABB"].push_back(j);
        ++ind;
    }


    nlohmann::write_json(relativeFilename, myJson, 4);
}

void VirtualRobotToSRLibGeneralRobot::convertRobot(const VirtualRobot::RobotPtr& virtualRobot)
{
    // define a robot w.r.t. its root (base) frame with srLib.
    std::vector<VirtualRobot::RobotNodePtr> nodesSimox = rns->getAllRobotNodes();
    extractJointScrews(nodesSimox);
    extractLinks(nodesSimox);

    // tool & tcp
    SE3 Mtool;
    SE3 Mtcp;
    generalRobot::InertiaTriple Gtool;
    extractTCPs(virtualRobot, Mtool, Mtcp, Gtool);

    // *********  FT sensor  ********* //
    auto ftSensor = getChildSensor(nodesSimox.back());
    //    SE3 M_ftSensor = EigenToSE3(convertToM(tcpPtr->getGlobalPose()))%EigenToSE3(convertToM(ftSensor->getGlobalPose()));
    Eigen::Matrix4f ftSensorGlobal = ftSensor->getGlobalPose();
    Eigen::Matrix4f ftParentGlobal = nodesSimox.back()->getGlobalPose();
    Eigen::Vector3f ftParentCom = getChildNotSensor(nodesSimox.back())->getCoMLocal();
    Eigen::Matrix4f Ji = Eigen::Matrix4f::Identity();
    math::Helpers::Position(Ji) = ftParentCom;
    ftParentGlobal = ftParentGlobal * Ji;

    SE3 M_parent_ft = (EigenToSE3(convertToM(ftParentGlobal)))%EigenToSE3(convertToM(ftSensorGlobal));
    M_tcp_ft = EigenToSE3(convertToM(rns->getTCP()->getGlobalPose())) % EigenToSE3(convertToM(ftSensorGlobal));
    //    std::cout << "parent : " << ftSensor->getParent()->getName() << std::endl;
    //    std::cout << "parent to ft : " << M_parent_ft;
    //    std::cout << "ftsensor node name: " << ftSensor->getName() << std::endl;
    //    std::cout << "from tcp: " << std::endl << M_ftSensor << std::endl;

    gRobot = new generalRobot(SE3());
    gRobot->setMLinks(robotLinks.M);
    gRobot->setScrews(jointScrews.screws);
    gRobot->setJointProperties(jointScrews.isJoint);
    gRobot->setInertias(robotLinks.inertias);
    gRobot->addTool(Mtool, Gtool);
    gRobot->addTCP(Mtcp);
    gRobot->setFTSensor(M_parent_ft);

    gRobot->initializeRobot();

    gRobot->setUpperLimits(jointScrews.jointUpperLimits);
    gRobot->setLowerLimits(jointScrews.jointLowerLimits);

}

void VirtualRobotToSRLibGeneralRobot::extractJointScrews(const std::vector<VirtualRobot::RobotNodePtr> &nodesSimox)
{
    // screw = [w; v]. w: rotational screw,     v: linear screw
    se3 temp_screw;
    se3 zero_screw(0., 0., 0., 0., 0., 0.);

    JointScrew tempScrews;
    for (std::size_t i = 0; i < nodesSimox.size(); ++i)
    {
        temp_screw = zero_screw;
        if (nodesSimox.at(i)->isRotationalJoint())
        {
            auto rotJointNode = std::dynamic_pointer_cast<VirtualRobot::RobotNodeRevolute>(nodesSimox.at(i));
            Eigen::Vector3f jointAxis = rotJointNode->getJointRotationAxisInJointCoordSystem();
            jointAxis.normalize();
            temp_screw = se3(jointAxis(0), jointAxis(1), jointAxis(2), 0, 0, 0);    // screw expressed in joint frame

            Eigen::Vector3f com = getChildNotSensor(nodesSimox.at(i))->getCoMLocal();
            Eigen::Matrix4f Ji = Eigen::Matrix4f::Identity();
            math::Helpers::Position(Ji) = -com;
            temp_screw = Ad(EigenToSE3(convertToM(Ji)), temp_screw);

            tempScrews.isJoint.push_back(true);
        }
        else if (nodesSimox.at(i)->isTranslationalJoint())
        {
            auto transJointNode = std::dynamic_pointer_cast<VirtualRobot::RobotNodePrismatic>(nodesSimox.at(i));
            Eigen::Vector3f jointAxis = transJointNode->getJointTranslationDirectionJointCoordSystem();
            jointAxis.normalize();
            temp_screw = se3(0, 0, 0, jointAxis(0), jointAxis(1), jointAxis(2));    // screw expressed in joint frame

            Eigen::Vector3f com = getChildNotSensor(nodesSimox.at(i))->getCoMLocal();
            Eigen::Matrix4f Ji = Eigen::Matrix4f::Identity();
            math::Helpers::Position(Ji) = -com;
            temp_screw = Ad(EigenToSE3(convertToM(Ji)), temp_screw);

            tempScrews.isJoint.push_back(true);
        }
        else
        {
            tempScrews.isJoint.push_back(false);
        }

        if (tempScrews.isJoint.back())
        {
            tempScrews.screws.push_back(temp_screw);
            tempScrews.jointUpperLimits.push_back(static_cast<double>(nodesSimox.at(i)->getJointLimitHigh()));
            tempScrews.jointLowerLimits.push_back(static_cast<double>(nodesSimox.at(i)->getJointLimitLow()));
            tempScrews.jointVelocityUpperLimits.push_back(static_cast<double>(nodesSimox.at(i)->getMaxVelocity()));
        }
    }

    jointScrews = tempScrews;

}

void VirtualRobotToSRLibGeneralRobot::extractLinks(const std::vector<VirtualRobot::RobotNodePtr> &nodesSimox)
{
    // up to hand's link frame
    std::vector<SE3> M;
    std::vector<SE3> MJ;

    std::vector<generalRobot::InertiaTriple> inertias;

    for (std::size_t i = 0; i < nodesSimox.size(); ++i)
    {
        // ***** inertia  ****** //
        // assume that body frame and the frame where inertia is calculated have the same orientation.
        generalRobot::InertiaTriple tempInertia;
        VirtualRobot::SceneObjectPtr childLink = getChildNotSensor(nodesSimox.at(i));

        VirtualRobot::CollisionModelPtr colModel = childLink->getCollisionModel();
        //        ARMARX_INFO << "COL MODEL CHILD: " << colModel->getName();
        VirtualRobot::BoundingBox bBox = colModel->getBoundingBox(false);
        //        ARMARX_INFO << "BOUNDING BOX MAX: " << bBox.getMax();
        //        ARMARX_INFO << "BOUNDING BOX MIN: " << bBox.getMax();
        //        ARMARX_INFO << "BOUNDING BOX CENTER: " << (bBox.getMax() + bBox.getMin()) / 2.0f;
        //        ARMARX_INFO << "BOUNDING BOX LENGTH: " << (bBox.getMax() - bBox.getMin()).array().abs();
        BoundingBoxInfo linkBB;
        linkBB.com = childLink->getCoMLocal() * 0.001f;
        linkBB.maxValues = bBox.getMax() * 0.001f;
        linkBB.minValues = bBox.getMin() * 0.001f;
        linkBoundingBox.push_back(linkBB);

        // mass
        tempInertia.m = static_cast<double>(childLink->getMass());

        // rotational inertia
        Eigen::Matrix3f inertialMatrix = childLink->getInertiaMatrix();
        tempInertia.I.push_back(inertialMatrix(0,0));  // ixx
        tempInertia.I.push_back(inertialMatrix(1,1));  // iyy
        tempInertia.I.push_back(inertialMatrix(2,2));  // izz
        tempInertia.I.push_back(inertialMatrix(0,1));  // ixy
        tempInertia.I.push_back(inertialMatrix(0,2));  // ixz
        tempInertia.I.push_back(inertialMatrix(1,2));  // iyz

        // c.o.m
        Eigen::Vector3f comi = childLink->getCoMLocal();     // local frame (joint frame) to c.o.m.  unit: [mm]
        tempInertia.r.push_back(0.0f);           // assume the inertia that is written in xml file is calculated at com.
        tempInertia.r.push_back(0.0f);           // Here, we define that body frame is attatched at the com.
        tempInertia.r.push_back(0.0f);

        inertias.push_back(tempInertia);

        // ***** link frame ***** //
        // body frames are attached at link's com.
        // In armarx, body frames are attached at joint frame.
        SE3 tempT;      // (i-1)-th joint frame to (i)-th joint frame
        SE3 tempM;      // (i-1)-th link frame to (i)-th link frame
        if (i == 0)
        {
            Eigen::Matrix4f Ti = nodesSimox.at(i)->getPoseInRootFrame();
            Eigen::Matrix4f Ji = Eigen::Matrix4f::Identity();
            math::Helpers::Position(Ji) = comi;

            tempT = EigenToSE3(convertToM(Ti));
            tempM = EigenToSE3(convertToM(Ti * Ji));
        }
        else
        {
            Eigen::Matrix4f Ti_1 = nodesSimox.at(i-1)->getGlobalPose();
            Eigen::Matrix4f Ti = nodesSimox.at(i)->getGlobalPose();
            Eigen::Matrix4f Ji_1 = Eigen::Matrix4f::Identity();
            Eigen::Matrix4f Ji = Eigen::Matrix4f::Identity();
            //            Eigen::Vector3f comi_1(inertias.at(i-1).r.at(0), inertias.at(i-1).r.at(1), inertias.at(i-1).r.at(2));
            Eigen::Vector3f comi_1 = getChildNotSensor(nodesSimox.at(i-1))->getCoMLocal();
            //            Eigen::Vector3f comi(inertias.at(i).r.at(0), inertias.at(i).r.at(1), inertias.at(i).r.at(2));
            math::Helpers::Position(Ji_1) = comi_1;
            math::Helpers::Position(Ji) = comi;

            tempT = EigenToSE3(convertToM(Ti_1)) % EigenToSE3(convertToM(Ti));
            tempM = EigenToSE3(convertToM(Ti_1 * Ji_1)) % EigenToSE3(convertToM(Ti * Ji));
        }
        //        M0i = M0i * tempM;
        MJ.push_back(tempT);
        M.push_back(tempM);
    }

    robotLinks.M = M;
    robotLinks.MJ = MJ;
    robotLinks.inertias = inertias;
}

void VirtualRobotToSRLibGeneralRobot::extractTCPs(const VirtualRobot::RobotPtr& virtualRobot, SE3 &Mtool, SE3 &Mtcp, generalRobot::InertiaTriple &inertiaTool)
{
    if (robotNodeSetName == "RightArm")
    {
        VirtualRobot::RobotNodePtr tcp = rns->getTCP();
        VirtualRobot::RobotNodePtr handColNode = virtualRobot->getRobotNode("Hand R Col");
        VirtualRobot::SceneObjectPtr lastLink = getChildNotSensor(rns->getAllRobotNodes().back());

        // ArmR8_Wri2 & ArmR8_Wri2_body have the identical reference frame
        //        Eigen::Matrix4f Tn = rns->getAllRobotNodes().back()->getGlobalPose();   // ArmR8_Wri2
        Eigen::Matrix4f Tn = lastLink->getGlobalPose();     // ArmR8_Wri2_body

        Eigen::Matrix4f Jn = Eigen::Matrix4f::Identity();
        Eigen::Vector3f com = lastLink->getCoMLocal();     // local frame (joint frame) to c.o.m.  unit: [mm]
        math::Helpers::Position(Jn) = com;

        Eigen::Matrix4f Ttool = tcp->getParent()->getParent()->getGlobalPose();
        Eigen::Matrix4f Jtool = Eigen::Matrix4f::Identity();
        Eigen::Vector3f comTool = handColNode->getCoMLocal();
        math::Helpers::Position(Jtool) = comTool;

        Mtool = EigenToSE3(convertToM(Tn * Jn)) % EigenToSE3(convertToM(Ttool * Jtool));
        Mtcp = EigenToSE3(convertToM(Ttool * Jtool)) % EigenToSE3(convertToM(tcp->getGlobalPose()));

        TCPInfo defaultTCP;
        defaultTCP.tcpName = tcp->getName();
        defaultTCP.tcpPoseLocal = Mtcp;
        robotTCPs.push_back(defaultTCP);

        std::vector<VirtualRobot::SceneObjectPtr> tcpSet = virtualRobot->getRobotNode("Hand R Root")->getChildren();
        tcpSet.at(0)->getName();
        tcpSet.at(0)->getGlobalPose();
        for (const auto& tempTCP : tcpSet)
        {
            if (tempTCP->getName().find("TCP") != std::string::npos)
            {
                // found!
                TCPInfo extraTCP;
                extraTCP.tcpName = tempTCP->getName();
                extraTCP.tcpPoseLocal = EigenToSE3(convertToM(Ttool * Jtool)) % EigenToSE3(convertToM(tempTCP->getGlobalPose()));
                robotTCPs.push_back(extraTCP);
            }
            else if (tempTCP->getName().find("GCP") != std::string::npos)
            {
                // found!
                TCPInfo extraTCP;
                extraTCP.tcpName = tempTCP->getName();
                extraTCP.tcpPoseLocal = EigenToSE3(convertToM(Ttool * Jtool)) % EigenToSE3(convertToM(tempTCP->getGlobalPose()));
                robotTCPs.push_back(extraTCP);
            }
        }


        // Inertia
        // volume, com and inertia are calculated by "trimesh" library in python.
        // used model: 'armar6-right-hand-full-col.stl'
        // com: [ 0.12103646, -0.00456164,  0.00421856]
        // inertia
        //        [ 1.65316844e-06, -1.21003287e-07, -3.35130406e-07],
        //        [-1.21003287e-07,  6.51630515e-06, -2.08191020e-07],
        //        [-3.35130406e-07, -2.08191020e-07,  5.62507516e-06]
        inertiaTool.m = handColNode->getMass();
        double volume = 0.0010867106284391935;
        double rho = inertiaTool.m / volume;

        Inertia tempG(1.65316844e-06, 6.51630515e-06, 5.62507516e-06, -1.21003287e-07, -3.35130406e-07, -2.08191020e-07);
        tempG *= rho;

        Eigen::Vector3f calculatedComLocal(0.12103646, -0.00456164,  0.00421856);
        Eigen::Vector3f tempT_position = comTool * 0.001f - calculatedComLocal;

        inertiaTool.I.push_back(tempG[0]);    // Ixx
        inertiaTool.I.push_back(tempG[1]);    // Iyy
        inertiaTool.I.push_back(tempG[2]);    // Izz
        inertiaTool.I.push_back(tempG[3]);    // Ixy
        inertiaTool.I.push_back(tempG[4]);    // Ixz
        inertiaTool.I.push_back(tempG[5]);    // Iyz

        inertiaTool.r.push_back(-tempT_position(0));
        inertiaTool.r.push_back(-tempT_position(1));
        inertiaTool.r.push_back(-tempT_position(2));



        // collision model bounding box
        VirtualRobot::CollisionModelPtr colModel = handColNode->getCollisionModel();
        VirtualRobot::BoundingBox bBox = colModel->getBoundingBox(false);
        BoundingBoxInfo linkBB;
        linkBB.com = comTool * 0.001f;
        linkBB.maxValues = bBox.getMax() * 0.001f;
        linkBB.minValues = bBox.getMin() * 0.001f;
        linkBoundingBox.push_back(linkBB);

    }
    else if (robotNodeSetName == "LeftArm")
    {
        VirtualRobot::RobotNodePtr tcp = rns->getTCP();
        VirtualRobot::RobotNodePtr handColNode = virtualRobot->getRobotNode("Hand L Col");
        VirtualRobot::SceneObjectPtr lastLink = getChildNotSensor(rns->getAllRobotNodes().back());

        Eigen::Matrix4f Tn = lastLink->getGlobalPose();
        Eigen::Matrix4f Jn = Eigen::Matrix4f::Identity();
        Eigen::Vector3f com = lastLink->getCoMLocal();     // local frame (joint frame) to c.o.m.  unit: [mm]
        math::Helpers::Position(Jn) = com;

        Eigen::Matrix4f Ttool = tcp->getParent()->getParent()->getGlobalPose();
        Eigen::Matrix4f Jtool = Eigen::Matrix4f::Identity();
        Eigen::Vector3f comTool = handColNode->getCoMLocal();
        math::Helpers::Position(Jtool) = comTool;

        Mtool = EigenToSE3(convertToM(Tn * Jn)) % EigenToSE3(convertToM(Ttool * Jtool));
        Mtcp = EigenToSE3(convertToM(Ttool * Jtool)) % EigenToSE3(convertToM(tcp->getGlobalPose()));

        TCPInfo defaultTCP;
        defaultTCP.tcpName = tcp->getName();
        defaultTCP.tcpPoseLocal = Mtcp;
        robotTCPs.push_back(defaultTCP);

        std::vector<VirtualRobot::SceneObjectPtr> tcpSet = virtualRobot->getRobotNode("Hand L Root")->getChildren();
        tcpSet.at(0)->getName();
        tcpSet.at(0)->getGlobalPose();
        for (const auto& tempTCP : tcpSet)
        {
            if (tempTCP->getName().find("TCP") != std::string::npos)
            {
                // found!
                TCPInfo extraTCP;
                extraTCP.tcpName = tempTCP->getName();
                extraTCP.tcpPoseLocal = EigenToSE3(convertToM(Ttool * Jtool)) % EigenToSE3(convertToM(tempTCP->getGlobalPose()));
                robotTCPs.push_back(extraTCP);
            }
            else if (tempTCP->getName().find("GCP") != std::string::npos)
            {
                // found!
                TCPInfo extraTCP;
                extraTCP.tcpName = tempTCP->getName();
                extraTCP.tcpPoseLocal = EigenToSE3(convertToM(Ttool * Jtool)) % EigenToSE3(convertToM(tempTCP->getGlobalPose()));
                robotTCPs.push_back(extraTCP);
            }
        }


        // Inertia
        // volume, com and inertia are calculated by "trimesh" library in python.
        // used model: 'armar6-right-hand-full-col.stl'
        // com: [  0.11537462, -0.00457567, -0.00358311]
        // inertia
        //        [ 1.55624157e-06, -1.18673242e-07,  3.20735289e-07],
        //        [-1.18673242e-07,  5.85355467e-06,  1.67980747e-07],
        //        [ 3.20735289e-07,  1.67980747e-07,  5.02622000e-06]
        inertiaTool.m = handColNode->getMass();
        double volume = 0.0009842678739312577;
        double rho = inertiaTool.m / volume;

        Inertia tempG(1.55624157e-06, 5.85355467e-06, 5.02622000e-06, -1.18673242e-07, 3.20735289e-07, 1.67980747e-07);
        tempG *= rho;

        Eigen::Vector3f calculatedComLocal(0.11537462, -0.00457567,  -0.00358311);
        Eigen::Vector3f tempT_position = comTool * 0.001f - calculatedComLocal;

        inertiaTool.I.push_back(tempG[0]);    // Ixx
        inertiaTool.I.push_back(tempG[1]);    // Iyy
        inertiaTool.I.push_back(tempG[2]);    // Izz
        inertiaTool.I.push_back(tempG[3]);    // Ixy
        inertiaTool.I.push_back(tempG[4]);    // Ixz
        inertiaTool.I.push_back(tempG[5]);    // Iyz

        inertiaTool.r.push_back(-tempT_position(0));
        inertiaTool.r.push_back(-tempT_position(1));
        inertiaTool.r.push_back(-tempT_position(2));



        // collision model bounding box
        VirtualRobot::CollisionModelPtr colModel = handColNode->getCollisionModel();
        VirtualRobot::BoundingBox bBox = colModel->getBoundingBox(false);
        BoundingBoxInfo linkBB;
        linkBB.com = comTool * 0.001f;
        linkBB.maxValues = bBox.getMax() * 0.001f;
        linkBB.minValues = bBox.getMin() * 0.001f;
        linkBoundingBox.push_back(linkBB);
    }
    else
    {
        ARMARX_IMPORTANT << "UNDEFINED ROBOT NODESET NAME.";
    }
}

VirtualRobot::SceneObjectPtr VirtualRobotToSRLibGeneralRobot::getChildNotSensor(const VirtualRobot::RobotNodePtr &currentNode)
{
    VirtualRobot::SceneObjectPtr childLink;
    for (const auto& child : currentNode->getChildren())
    {
        auto childSensors = currentNode->getSensors();
        int sCount = 0;
//        ARMARX_INFO << "CHILD NAME: " << child->getName();
        for (const auto& childSensor : childSensors)
        {
//            ARMARX_INFO << "CHILD SENSOR NAME: " << childSensor->getName();
            if (child->getName().compare(childSensor->getName()) == 0)
            {
                ++sCount;
            }
        }
        if (child->getName().find("FT") != std::string::npos)
        {
            ++sCount;
        }

        if (sCount == 0)
        {
            childLink = child;
        }
    }

    return childLink;
}

VirtualRobot::SceneObjectPtr VirtualRobotToSRLibGeneralRobot::getChildSensor(const VirtualRobot::RobotNodePtr &currentNode)
{
    /// Find FT senrsor
    VirtualRobot::SceneObjectPtr childLink;
//    ARMARX_INFO << "GET CHILD SENSOR: " << currentNode->getName();
    for (const auto& childs : currentNode->getChildren())
    {
//        ARMARX_INFO << childs->getName();
        auto childSensors = currentNode->getSensors();
        int scount = 0;
        for (const auto& childSensor : childSensors)
        {
//            ARMARX_INFO << "GET CHILDE SENSOR -- SENSOR NAME: " << childSensor->getName();
            if (childs->getName().compare(childSensor->getName()) == 0)
            {
                ++scount;
//                ARMARX_INFO << "get child sensor -- " << scount << " : " << childSensor->getName();
            }
        }

        // FTsensor is not counted as a sensor. (2022.08.22.)
        if (/*scount > 0 && */childs->getName().find("FT") != std::string::npos)
        {
            childLink = childs;
        }
    }

    return childLink;
}

}
