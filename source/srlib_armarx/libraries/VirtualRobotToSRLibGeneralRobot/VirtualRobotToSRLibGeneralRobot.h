/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    srLibArmarX::ArmarXObjects::VirtualRobotToSRLibGeneralRobot
 * @author     Byungchul An ( byungchul dot an at gmail dot com )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <Eigen/Core>

#include <srlib_armarx/libraries/srLib/generalRobot.h>
#include <VirtualRobot/Robot.h>

namespace armarx
{
    struct JointScrew
    {
        std::vector<se3> screws;
        std::vector<bool> isJoint;
        std::vector<double> jointUpperLimits;
        std::vector<double> jointLowerLimits;
        std::vector<double> jointVelocityUpperLimits;
    };

    struct LinkInfo
    {
        std::vector<SE3> M;     // {i-1}-th link frame to {i}-th link frame
        std::vector<SE3> MJ;     // {i-1}-th joint frame to {i}-th joint frame
        std::vector<generalRobot::InertiaTriple> inertias;  // inertia w.r.t. ???
    };

    struct TCPInfo
    {
        std::string tcpName;
        SE3 tcpPoseLocal;   // SE3 from {Hand R Base} (or Hand R Root) to tcp in case of RightArm.
    };

    struct BoundingBoxInfo
    {
        Eigen::Vector3f maxValues;
        Eigen::Vector3f minValues;
        Eigen::Vector3f com;
    };

    /**
    * @defgroup Library-VirtualRobotToSRLibGeneralRobot VirtualRobotToSRLibGeneralRobot
    * @ingroup srLibArmarX
    * A description of the library VirtualRobotToSRLibGeneralRobot.
    *
    * @class VirtualRobotToSRLibGeneralRobot
    * @ingroup Library-VirtualRobotToSRLibGeneralRobot
    * @brief Brief description of class VirtualRobotToSRLibGeneralRobot.
    *
    * Detailed description of class VirtualRobotToSRLibGeneralRobot.
    */
    class VirtualRobotToSRLibGeneralRobot
    {
    public:
        VirtualRobotToSRLibGeneralRobot(const VirtualRobot::RobotPtr& virtualRobot, const std::string& _robotNodeSetName);
        ~VirtualRobotToSRLibGeneralRobot();

        SE3 EigenToSE3(const Eigen::Matrix4f& _in);
        Eigen::Matrix4f SE3ToEigen(const SE3& _in);
        Eigen::Matrix4f convertToM(const Eigen::Matrix4f& _in);     // convert [mm] to [m].
        //        Eigen::Matrix4f convertToMM(const Eigen::Matrix4f& _in);     // convert [m] to [mm].
        Eigen::MatrixXf convertInertia(const Inertia& M);

        bool forwardKinematics();
        bool forwardKinematics(const Eigen::VectorXf& _theta);
        Eigen::VectorXf inverseKinematics(const Eigen::Matrix4f& target, const Eigen::VectorXf& initialTheta);

        Eigen::Matrix4f getTCPFrame();     // In root frame
        Eigen::Matrix4f getTCPFrameInGlobal();
        Eigen::Matrix4f getLinkFrame(unsigned int index);

        Eigen::MatrixXf getBodyJacobian();
        Eigen::MatrixXf getBodyJacobianInRoot();    // It only changes the orientation as the root frame.
        Eigen::MatrixXf getBodyJacobianInGlobal();  // It only changes the orientation as the global frame.
        Eigen::MatrixXf getSpaceJacobian();         // space jacobian in root frame
        Eigen::MatrixXf getSpaceJacobianInGlobal(); // space jacobian in global frame

        void exportRobotParameters();


    private:
        generalRobot* gRobot = nullptr;
        VirtualRobot::RobotNodeSetPtr rns;
        std::string robotNodeSetName;
        SE3 baseFrame;     //  robot base (root) in Global Frame.
        SE3 M_tcp_ft;

        JointScrew jointScrews;     // screws are defined in the link frame. (i-th screw in i-th link frame (c.o.m. frame))
        LinkInfo robotLinks;
        std::vector<TCPInfo> robotTCPs;
        std::vector<BoundingBoxInfo> linkBoundingBox;
        Eigen::VectorXd* thetaPtr = nullptr;

        void convertRobot(const VirtualRobot::RobotPtr& virtualRobot);
        void extractJointScrews(const std::vector<VirtualRobot::RobotNodePtr>& nodesSimox);      // extract joint screws from virtual robot
        void extractLinks(const std::vector<VirtualRobot::RobotNodePtr>& nodesSimox);      // extract links from virtual robot
        void extractTCPs(const VirtualRobot::RobotPtr& virtualRobot, SE3& Mtool, SE3& Mtcp, generalRobot::InertiaTriple& inertiaTool);
        VirtualRobot::SceneObjectPtr getChildNotSensor(const VirtualRobot::RobotNodePtr& currentNode);
        VirtualRobot::SceneObjectPtr getChildSensor(const VirtualRobot::RobotNodePtr& currentNode);
    };

}
