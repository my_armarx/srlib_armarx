/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    srlib_armarx
 * @author     Byungchul An ( byungchul dot an at gmail dot com )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "srLib_armarx_converter.h"

namespace armarx {
namespace srLib {

SE3 EigenToSE3(const Eigen::Matrix4f &_in)
{
    SE3 temp = SE3(_in(0,0), _in(1, 0), _in(2,0), _in(0,1), _in(1, 1), _in(2,1),
                   _in(0,2), _in(1, 2), _in(2,2), _in(0,3), _in(1, 3), _in(2,3));
    return temp;
}

Eigen::Matrix4f SE3ToEigen(const SE3 &_in)
{
    Eigen::Matrix4f ret;
    ret.col(0) << _in[0], _in[1], _in[2], 0.0f;
    ret.col(1) << _in[3], _in[4], _in[5], 0.0f;
    ret.col(2) << _in[6], _in[7], _in[8], 0.0f;
    ret.col(3) << _in[9], _in[10], _in[11], 1.0f;

    return ret;
}

Eigen::VectorXf LogSE3(const Eigen::Matrix4f &T)
{
    SE3 M = EigenToSE3(T);
    se3 logM = Log(M);

    return se3ToEigen(logM);
}

se3 EigenTose3(const Eigen::VectorXf &V)
{
//    se3 S = se3(V[0], V[1], V[2], V[3], V[4], V[5]);

    return se3(V[3], V[4], V[5], V[0], V[1], V[2]);
}

Eigen::VectorXf se3ToEigen(const se3 &S)
{
    Eigen::VectorXf V(6);
    V << S[3], S[4], S[5], S[0], S[1], S[2];

    return V;
}

Eigen::VectorXf changese3Order(const Eigen::VectorXf &S)
{
    Eigen::VectorXf V(6);
    V << S[3], S[4], S[5], S[0], S[1], S[2];

    return V;
}

Eigen::Matrix3f skew(const Eigen::Vector3f &w)
{
    Eigen::Matrix3f W;
    W <<    0.0f, -w[2], w[1],
            w[2], 0.0f, -w[0],
            -w[1], w[0], 0.0f;

    return W;
}

Eigen::Matrix4f ExpSE3(const Eigen::VectorXf &V)
{
    SE3 M = Exp(EigenTose3(V));
    return SE3ToEigen(M);
}

Eigen::MatrixXf Ad(const Eigen::Matrix4f &T)
{
    /// calculate an adjoint matrix.
    /// V_a = Ad(T_ab) * V_b
    /// V = (v, w),  v: linear, w: angular

    Eigen::Matrix3f R = T.block(0, 0, 3, 3);
    Eigen::Matrix3f A;

    A(0, 0) = -T(2, 3)*T(1, 0) + T(1, 3)*T(2, 0);
    A(0, 1) = -T(2, 3)*T(1, 1) + T(1, 3)*T(2, 1);
    A(0, 2) = -T(2, 3)*T(1, 2) + T(1, 3)*T(2, 2);

    A(1, 0) = -T(0, 3)*T(2, 0) + T(2, 3)*T(0, 0);
    A(1, 1) = -T(0, 3)*T(2, 1) + T(2, 3)*T(0, 1);
    A(1, 2) = -T(0, 3)*T(2, 2) + T(2, 3)*T(0, 2);

    A(2, 0) = -T(1, 3)*T(0, 0) + T(0, 3)*T(1, 0);
    A(2, 1) = -T(1, 3)*T(0, 1) + T(0, 3)*T(1, 1);
    A(2, 2) = -T(1, 3)*T(0, 2) + T(0, 3)*T(1, 2);


    Eigen::MatrixXf adjoint(6, 6);
    adjoint.topLeftCorner(3, 3) = R;
    adjoint.topRightCorner(3, 3) = A;
    adjoint.bottomLeftCorner(3, 3) = Eigen::Matrix3f::Zero();
    adjoint.bottomRightCorner(3, 3) = R;

    return adjoint;
}

Eigen::MatrixXf dAd(const Eigen::Matrix4f &T)
{
    /// calculate a dual adjoint matrix.
    /// dAd(T) = Ad(inv(T))^T
    /// F_a = dAd(T_ab) * F_b
    /// F = (force, moment)

    Eigen::Matrix3f R = T.block(0, 0, 3, 3);
    Eigen::Matrix3f A;

    A(0, 0) = -T(2, 3)*T(1, 0) + T(1, 3)*T(2, 0);
    A(0, 1) = -T(2, 3)*T(1, 1) + T(1, 3)*T(2, 1);
    A(0, 2) = -T(2, 3)*T(1, 2) + T(1, 3)*T(2, 2);

    A(1, 0) = -T(0, 3)*T(2, 0) + T(2, 3)*T(0, 0);
    A(1, 1) = -T(0, 3)*T(2, 1) + T(2, 3)*T(0, 1);
    A(1, 2) = -T(0, 3)*T(2, 2) + T(2, 3)*T(0, 2);

    A(2, 0) = -T(1, 3)*T(0, 0) + T(0, 3)*T(1, 0);
    A(2, 1) = -T(1, 3)*T(0, 1) + T(0, 3)*T(1, 1);
    A(2, 2) = -T(1, 3)*T(0, 2) + T(0, 3)*T(1, 2);


    Eigen::MatrixXf adjoint(6, 6);
    adjoint.topLeftCorner(3, 3) = R;
    adjoint.topRightCorner(3, 3) = Eigen::Matrix3f::Zero();
    adjoint.bottomLeftCorner(3, 3) = A;
    adjoint.bottomRightCorner(3, 3) = R;

    return adjoint;
}

Eigen::Matrix4f InverseSE3(const Eigen::Matrix4f &_in)
{
    Eigen::Matrix3f R = _in.block<3, 3>(0, 0);
    Eigen::Vector3f p = _in.block<3, 1>(0, 3);
    Eigen::Matrix4f out = Eigen::Matrix4f::Identity();
    out.block<3, 3>(0, 0) = R.transpose();
    out.block<3, 1>(0, 3) = -R.transpose() * p;

    return out;
}

Eigen::VectorXf ad(const Eigen::VectorXf &V1, const Eigen::VectorXf &V2)
{
    // V1, V2: written in (v, w)
    // s1, x2: written in (w, v)
    Eigen::VectorXf s1 = changese3Order(V1);
    Eigen::VectorXf s2 = changese3Order(V2);

    Eigen::VectorXf returnVec(6, 1);
    returnVec[0] = s1[1] * s2[2] - s1[2] * s2[1];
    returnVec[1] = s1[2] * s2[0] - s1[0] * s2[2];
    returnVec[2] = s1[0] * s2[1] - s1[1] * s2[0];
    returnVec[3] = s1[1] * s2[5] - s1[2] * s2[4] - s2[1] * s1[5] + s2[2] * s1[4];
    returnVec[4] = s1[2] * s2[3] - s1[0] * s2[5] - s2[2] * s1[3] + s2[0] * s1[5];
    returnVec[5] = s1[0] * s2[4] - s1[1] * s2[3] - s2[0] * s1[4] + s2[1] * s1[3];

    return changese3Order(returnVec);
}


}

}
