#ifndef _SR_GENERAL_ROBOT_
#define _SR_GENERAL_ROBOT_

#include <string>
#include <vector>
#include <map>

#include "utils.h"

#include "LieGroup.h"
#include "Eigen/Dense"
#include "Eigen/Sparse"

//using namespace std;

class generalRobot
{
public:
    struct InertiaTriple
    {
        std::vector<double> I;      // inertia at c.o.m. orientation aligned to link frame.
        std::vector<double> r;      // local to c.o.m.
        double m;
    };
public:
	generalRobot(SE3 _base);
	~generalRobot();

	unsigned int getDof();
	unsigned int getNumConnect();
	double getReductionRatio();

	SE3 getBaseFrame();
	SE3 getLinkFrame(unsigned int);
	SE3 getToolFrame();
    SE3 getFTSensorFrame();
    std::vector<SE3> getLinkTransform();
    SE3 getTCPTransform();
	
	vector<bool> getConnectionStatus();

	Eigen::VectorXd* getThetaPointer();
	Eigen::VectorXd* getThetaDotPointer();
	Eigen::VectorXd getInitTheta();


	Eigen::VectorXd getUpperLimits();
	Eigen::VectorXd getLowerLimits();
	bool checkJointLimit();
	
	vector<se3> getScrews();
	vector<Inertia> getInertias();
	Eigen::MatrixXd getSpaceJacobian();
	Eigen::MatrixXd getToolJacobian();
	Eigen::MatrixXd getAnalJacobian();
	Eigen::MatrixXd getBodyJacobianAtTc(SE3 _T_c, int _idx);
	Eigen::MatrixXd getBodyJacobianAtCP(int _idx_cyl, int _idx_CP);
	Eigen::MatrixXd* getDerivativeToolJacobian();

	// Kinematics
    bool forwardKinematics(Eigen::VectorXd* _theta = nullptr);     // it does not update m_theta
	Eigen::VectorXd inverseKinematics(SE3 _T_target, Eigen::VectorXd* _theta=NULL);

	// Dynamics

	Eigen::MatrixXd* getA();
	void setdA(Eigen::MatrixXd* _A);
	Eigen::MatrixXd* getdA(Eigen::MatrixXd* _A);
	Eigen::MatrixXd getMassfromA(Eigen::MatrixXd* _A);
	Eigen::MatrixXd getMassDer(Eigen::MatrixXd* _A, int _k);
	void setMassMatrix();
	Eigen::MatrixXd getMassMatrix();
	Eigen::VectorXd inverseDynamics(Eigen::VectorXd* _theta_ddot = NULL, Eigen::VectorXd* _theta = NULL, Eigen::VectorXd* _theta_dot = NULL);
	Eigen::VectorXd forwardDynamics(Eigen::VectorXd* _tau, Eigen::VectorXd* _theta = NULL, Eigen::VectorXd* _theta_dot = NULL);

    vector<bool> m_is_activeSlave;      // currently not used.

	int m_num_cylder;
	// variables for collision
	SE3* m_cylder_frame_init;
	SE3* m_cylder_frame;
	double* m_cylder_length;
	double* m_cylder_radius;
	int* m_cylder_prnt_jnt;
	SE3 m_M_tool;

    void addScrew(const se3&);
    void setScrews(const std::vector<se3>& );
    void addMLink(const SE3&);                  // initial link frame
    void addMLink(const SO3&, const Vec3&);
    void setMLinks(const std::vector<SE3>&);
    void addInertia(const double&, const std::vector<double>&, const std::vector<double>&);
    void addInertia(const InertiaTriple&);
    void setInertias(const std::vector<InertiaTriple>&);
    void setUpperLimits(const std::vector<double>&);
    void setLowerLimits(const std::vector<double>&);
    void setVelocityUpperLimits(const std::vector<double>&);
    void setJointProperties(const std::vector<bool>&);
    void addTool(const SE3&, const InertiaTriple&);
    void addTCP(const SE3&);
    void setFTSensor(const SE3&);

    void initializeRobot();

    void setKinematics_INDY7();

    void exportRobot(const std::string& output_FileName);

private:


	SO3  FindOrientation(const char*, const char*, const char*, const char*);
	void resolveAngle(Eigen::VectorXd *);



	//variables
	int m_dof;
	int m_num_connect;
	double m_reducRatio;
	
	vector<bool> m_is_joint;
	vector<int> m_dof_connection;
    vector<SE3> m_M_links;              // M_i-1_i
    vector<se3> m_screws;               // A_i : screw axis w.r.t. link {i} frame

	SE3 m_T_base;

	SE3 m_T_tool;
    SE3 m_M_FTSensor;                   // from tcp to ft sensor frame
	
    SE3* m_T_links;                     // T_0_i

	Eigen::VectorXd* m_initTheta;

	Eigen::VectorXd* m_theta;
	Eigen::VectorXd* m_theta_dot;
	Eigen::VectorXd* m_ddot_theta;

	Eigen::VectorXd* m_theta_limit_upper;
	Eigen::VectorXd* m_theta_limit_lower;
    Eigen::VectorXd* m_theta_dot_limit_upper;

	//srMatrix* m_Jacobian_space;

	//variables for dynamics
	vector<Inertia> m_link_inertia;
	vector<double> m_link_mass;
	Eigen::MatrixXd m_mass_matrix;
	Eigen::VectorXd m_dV0,m_V0;
	Eigen::MatrixXd *m_dA;
	//SE3* m_T_com;
	//sr_real* m_k_v;
	//sr_real* m_k_c;

	int* m_num_controlPoints;
	double** m_controlPoints_pos;

};

Eigen::VectorXd se3ToVectorXd(const se3 &);
Eigen::Vector3d Vec3ToVector3d(Vec3);
Eigen::MatrixXd Ad(SE3);
Eigen::VectorXd ad(Eigen::VectorXd, Eigen::VectorXd);
Eigen::VectorXd Ad(const SE3 &T, const Eigen::VectorXd &s);
Eigen::VectorXd dad(const Eigen::VectorXd &s, const Eigen::VectorXd &t);
Eigen::VectorXd dAd(const SE3 &T, const Eigen::VectorXd &t);

Vec3 operator+(Eigen::VectorXd, Vec3);
Eigen::MatrixXd operator*(SO3, Axis);
Eigen::MatrixXd operator*(SE3, se3);
Eigen::MatrixXd operator*(Eigen::MatrixXd, SO3);
Eigen::MatrixXd operator*(SO3, Eigen::MatrixXd);
Eigen::MatrixXd operator*(Eigen::MatrixXd, SE3);
Eigen::MatrixXd operator*(Eigen::MatrixXd, Vec3);
Eigen::MatrixXd operator*(SE3, Eigen::MatrixXd);
Eigen::VectorXd operator*(Inertia, Eigen::VectorXd);
Eigen::VectorXd operator-(Eigen::VectorXd p, Vec3 q);
sr_real operator*(se3,Eigen::VectorXd);

sr_real Inner(Eigen::VectorXd &p, const Vec3 &q);
Eigen::MatrixXd Skew(Vec3 q);
Eigen::MatrixXd Skew(se3 q);
#endif
