#include "generalRobot.h"
#include <fstream>
#include <iomanip>
#include <iostream>

generalRobot::generalRobot(SE3 _base)
{
	m_T_base = _base;
	m_V0.setZero(6);
	m_dV0.setZero(6);
    m_dV0[5] = 9.81;

//    std::cout << "constructor!! \n";
//    setKinematics_INDY7();

//	switch (_robotType)
//	{
//
//	case _SET_UR_:
//		setKinematics_UR(6);
//		break;
//	case _SET_MODMAN4_:
//		setKinematics_XML(_path);
//		break;
//	default:
//		cout <<"Wrong ID"<< endl;
//		return;
//		break;
//	}
}

generalRobot::~generalRobot()
{
	SR_SAFE_DELETE_AR(m_T_links);
	SR_SAFE_DELETE(m_theta);
	SR_SAFE_DELETE(m_theta_limit_upper);
	SR_SAFE_DELETE(m_theta_limit_lower);
    SR_SAFE_DELETE(m_theta_dot_limit_upper)

	//if assigned

	//delete[] m_dot_theta;
	//delete[] m_ddot_theta;
	//delete[] m_link_inertia;
	//delete[] m_rotor_inertia;
	//delete[] m_T_com;
	//delete m_k_v; 
	//delete m_k_c;
}


void generalRobot::setKinematics_INDY7()
{
	Inertia tempInertia;
	SE3 temp_SE3;


	se3 temp_se3;

	//KINEMATICS

	temp_se3 = se3(0, 0, 1, 0, 0, 0);
//	m_screws.push_back(temp_se3);
    addScrew(temp_se3);
    temp_SE3 = SE3(SO3(), Vec3(0, 0, 0.079));
//	m_M_links.push_back(SE3(SO3(), Vec3(0, 0, 0.079)));
    addMLink(temp_SE3);

	temp_se3 = se3(1, 0, 0, 0, 0, 0);
	m_screws.push_back(temp_se3);
	m_M_links.push_back(SE3(SO3(), Vec3(0.1105, 0, 0.221)));

	temp_se3 = se3(1, 0, 0, 0, 0, 0);
	m_screws.push_back(temp_se3);
	m_M_links.push_back(SE3(SO3(), Vec3(-0.0335, 0, 0.450)));

	temp_se3 = se3(0, 0, 1, 0, 0, 0);
	m_screws.push_back(temp_se3);
	m_M_links.push_back(SE3(SO3(), Vec3(-0.0735, 0, 0.2665)));

	temp_se3 = se3(1, 0, 0, 0, 0, 0);
	m_screws.push_back(temp_se3);
	m_M_links.push_back(SE3(SO3(), Vec3(0.1155, 0, 0.0835)));

	temp_se3 = se3(0, 0, 1, 0, 0, 0);
	m_screws.push_back(temp_se3);
	m_M_links.push_back(SE3(SO3(), Vec3(0.0675, 0, 0.1675)));


	//HAND
	// 1st year nuclear
	//m_cad_path.push_back("indy7/nuclearHand.obj");
	//temp_SE3 = RotY(SR_PI / 2)*RotX(SR_PI); temp_SE3.SetPosition(0,0, 0.0605);
	//m_M_links.push_back(temp_SE3);

	// 2nd year nuclear
//	m_cad_path.push_back("");
	temp_SE3 = RotZ(SR_PI_HALF);
	temp_SE3.SetPosition(Vec3(0., 0., 0.0605 + 0.036)); // link6 + ft_sensor
	m_M_links.push_back(temp_SE3);


	//DYNAMICS

	//SE3 temp_SE3_2;
	//temp_SE3_2.SetPosition(0.04732382, 0.00000000, 0.16345867);//1
	//cout.precision(6);
	//cout << fixed <<temp_SE3_2.GetPosition() - m_M_links[0].GetPosition()<<endl<<endl;
	//temp_SE3_2.SetPosition(0.16846928, 0.00000601, 0.50562772);//2
	//cout << fixed << temp_SE3_2.GetPosition() - (m_M_links[0] * m_M_links[1]).GetPosition() << endl << endl;
	//temp_SE3_2.SetPosition(0.00530544, 0.00004261, 0.85468888);//3
	//cout << fixed << temp_SE3_2.GetPosition() - (m_M_links[0] * m_M_links[1] * m_M_links[2]).GetPosition() << endl << endl;
	//temp_SE3_2.SetPosition(0.07568080, 0.00006707, 1.04455811);//4
	//cout << fixed << temp_SE3_2.GetPosition() - (m_M_links[0] * m_M_links[1] * m_M_links[2] * m_M_links[3]).GetPosition() << endl << endl;
	//temp_SE3_2.SetPosition(0.18327163, 0.00006250, 1.14576671);//5
	//cout << fixed << temp_SE3_2.GetPosition() - (m_M_links[0] * m_M_links[1] * m_M_links[2] * m_M_links[3] * m_M_links[4]).GetPosition() << endl << endl;
	//temp_SE3_2.SetPosition(0.18698743, 0.00000000, 1.25162342);//6
	//cout << fixed << temp_SE3_2.GetPosition() - (m_M_links[0] * m_M_links[1] * m_M_links[2] * m_M_links[3] * m_M_links[4] * m_M_links[5]).GetPosition() << endl << endl;

	tempInertia = Inertia(0.12937017, 0.15418559, 0.05964415, 0.00000235, 0.04854267, 0.00001739);
	tempInertia.SetMass(11.80301028);
	temp_SE3 = SE3(SO3(), -Vec3(0.0473238200, 0, 0.0844586700));
	tempInertia = tempInertia.Transform(temp_SE3);
	m_link_inertia.push_back(tempInertia);

	tempInertia = Inertia(0.25088420, 0.29356980, 0.03620519, 0.00000040, 0.03727972, -0.00001441);
	tempInertia.SetMass(7.99166746);
	temp_SE3 = SE3(SO3(), -Vec3(0.0579692800, 0.0000060100, 0.2056277200));
	tempInertia = tempInertia.Transform(temp_SE3);
	m_link_inertia.push_back(tempInertia);

	tempInertia = Inertia(0.03406024, 0.03424593, 0.00450477, -0.00000149, 0.00186009, -0.00000724);
	tempInertia.SetMass(2.98863839);
	temp_SE3 = SE3(SO3(), -Vec3(-0.0716945600, 0.0000426100, 0.1046888800));
	tempInertia = tempInertia.Transform(temp_SE3);
	m_link_inertia.push_back(tempInertia);

	tempInertia = Inertia(0.00278347, 0.00670405, 0.00617624, -0.00000375, -0.00127967, -0.00000150);
	tempInertia.SetMass(2.11700218);
	temp_SE3 = SE3(SO3(), -Vec3(0.0721808000, 0.0000670700, 0.0780581100));
	tempInertia = tempInertia.Transform(temp_SE3);
	m_link_inertia.push_back(tempInertia);

	tempInertia = Inertia(0.00978189, 0.00994891, 0.00271492, -0.00000014, -0.00093546, -0.00000321);
	tempInertia.SetMass(2.28305542);
	temp_SE3 = SE3(SO3(), -Vec3(0.0682716300, 0.0000625000, 0.0957667100));
	tempInertia = tempInertia.Transform(temp_SE3);
	m_link_inertia.push_back(tempInertia);

	tempInertia = Inertia(0.00044549, 0.00043534, 0.00059634, -0.00000013, 0.00000051, 0.00000002);
	tempInertia.SetMass(0.40083918);
	temp_SE3 = SE3(SO3(), -Vec3(0.0044874300, 0.0000000000, 0.0441234200));
	tempInertia = tempInertia.Transform(temp_SE3);
	m_link_inertia.push_back(tempInertia);


	//HAND
	// 1st year nuclear
	//tempInertia = Inertia(0.01723929581, 0.01498416937, 0.02112520587, -0.00202259557, -0.00040996833, -0.00030100144);

	//tempInertia.SetMass(4.09884);
	//temp_SE3 = SE3(SO3(), -Vec3(-0.08923, 0.01380, 0.00057));
	//tempInertia = tempInertia.Transform(temp_SE3);
	//m_link_inertia.push_back(tempInertia);

	// 2nd year nuclear
//	tempInertia = Inertia(0.00253442656, 0.00305175440, 0.00113342711, -0.00000167521, -0.00000109207, 0.00001654305);
//	tempInertia.SetMass(0.96780);
//	temp_SE3 = RotY(SR_PI/2)*RotZ(SR_PI/2);
//	temp_SE3.SetPosition(Vec3(-0.07440, -0.00006, -0.00044));

	tempInertia = Inertia(0.00269635139, 0.00328544648, 0.00122491254, -0.00000166796, -0.00000107140, 0.00001692790);
	tempInertia.SetMass(1.25154);
//	temp_SE3 = RotY(SR_PI/2)*RotZ(SR_PI/2);
	temp_SE3 = SE3();
//	temp_SE3.SetPosition(Vec3(-0.07615, -0.00005, -0.00036));
	temp_SE3.SetPosition(Vec3(0.00005, 0.00036, -0.07615));
	tempInertia = tempInertia.Transform(temp_SE3);
	m_link_inertia.push_back(tempInertia);

	m_dof = m_screws.size();

	m_initTheta = new Eigen::VectorXd(m_dof);
	m_initTheta->setZero(m_dof);

	//(*m_initTheta)[0] = 0;
	//(*m_initTheta)[1] = 0;
	//(*m_initTheta)[2] = 0;
	//(*m_initTheta)[3] = 0;
	//(*m_initTheta)[4] = 10.0*SR_PI/180.0;
	//(*m_initTheta)[5] = 0;

	m_theta_limit_upper = new Eigen::VectorXd(m_dof);
	m_theta_limit_lower = new Eigen::VectorXd(m_dof);

	m_reducRatio = 1;

	m_is_activeSlave.push_back(false);

	for (int i = 0; i < m_dof; ++i)
	{
		m_is_joint.push_back(true);
		m_dof_connection.push_back(1);
		m_is_activeSlave.push_back(true);

		(*m_theta_limit_lower)(i, 0) = -SR_PI;
		(*m_theta_limit_upper)(i, 0) = SR_PI;
	}
	m_is_activeSlave.push_back(false);

	m_is_joint.push_back(false);
	m_dof_connection.push_back(0);

//	m_M_tool = SE3(SO3(), Vec3(0, 0, 0.0605));		// knob only
	//m_M_tool = SE3(SO3(), Vec3(-0.15, 0, 0));		// 1st year nuclear
	m_M_tool = SE3(SO3(), Vec3(0., 0., 0.148));		// 2nd year nuclear. hand's link frame to grip point

	m_num_connect = m_M_links.size();

	m_T_links = new SE3[m_num_connect];
	m_theta = new Eigen::VectorXd(m_dof);
	m_theta_dot = new Eigen::VectorXd(m_dof, 1);
	m_theta_dot->setZero();

	*m_theta = *m_initTheta;

	SE3* M = new SE3[m_num_connect];

	M[0] = m_M_links[0];

	for (int i = 1; i < m_num_connect; ++i)
	{
		M[i] = M[i - 1] * m_M_links[i];
	}


	if(true)
	{
		m_num_cylder = 4;
		m_cylder_frame_init = new SE3[m_num_cylder];
		m_cylder_frame = new SE3[m_num_cylder];
		m_cylder_length = new double[m_num_cylder];
		m_cylder_radius = new double[m_num_cylder];
		m_cylder_prnt_jnt = new int[m_num_cylder];

		m_controlPoints_pos = new double*[m_num_cylder];
		m_num_controlPoints = new int[m_num_cylder];

		SO3 tmpSO3(1, 0, 0, 0, 0, 1, 0, -1, 0);

		m_cylder_frame_init[0] = SE3(tmpSO3, Vec3(0.19, 0.0, 0.5));
		m_cylder_frame_init[1] = SE3(tmpSO3, Vec3(-0.0,	  0.0, 0.9));
		m_cylder_frame_init[2] = SE3(tmpSO3, Vec3(0.1855, 0.0, 1.140));
		m_cylder_frame_init[3] = SE3(tmpSO3, Vec3(0.1855, 0.0, 1.3));

		m_cylder_length[0] = 0.541; m_cylder_length[1] = 0.37;  m_cylder_length[2] = 0.25;	m_cylder_length[3] = 0.05;
		m_cylder_radius[0] = 0.07;  m_cylder_radius[1] = 0.065; m_cylder_radius[2] = 0.055;	m_cylder_radius[3] = 0.05;
		m_cylder_prnt_jnt[0] = 1;   m_cylder_prnt_jnt[1] = 2;   m_cylder_prnt_jnt[2] = 4;	m_cylder_prnt_jnt[3] = 5;

		m_num_controlPoints[0] = 1; m_num_controlPoints[1] = 1; m_num_controlPoints[2] = 1; m_num_controlPoints[3] = 1;

		for (int i = 0; i < m_num_cylder; ++i)
		{
			m_controlPoints_pos[i] = new double[m_num_controlPoints[i]];
			m_controlPoints_pos[i][0] = 0;
		}

		for (int i = 0; i < m_num_cylder; ++i)
			m_cylder_frame_init[i] = Inv(M[m_cylder_prnt_jnt[i]])*m_cylder_frame_init[i];
	}
	else
	{
		m_num_cylder = 100;
		m_cylder_frame_init = new SE3[m_num_cylder];
		m_cylder_frame = new SE3[m_num_cylder];
		m_cylder_prnt_jnt = new int[m_num_cylder];

		m_controlPoints_pos = new double*[m_num_cylder];
		m_num_controlPoints = new int[m_num_cylder];

		SE3 tmpSE3(1, 0, 0, 0, 0, 1, 0, -1, 0, 0.1855, 0.0, 1.3);


		for(int i=0; i<m_num_cylder; ++i)
		{
			m_cylder_frame_init[i] = tmpSE3;
			m_cylder_prnt_jnt[i] = 5;
			m_num_controlPoints[i] = 1;

			m_controlPoints_pos[i] = new double[m_num_controlPoints[i]];
			m_controlPoints_pos[i][0] = 0;

			m_cylder_frame_init[i] = Inv(M[m_cylder_prnt_jnt[i]])*m_cylder_frame_init[i];
		}
	}



	delete[] M;

    forwardKinematics(m_theta);
}

void generalRobot::exportRobot(const string &output_FileName)
{
    std::ofstream writeFile;
    writeFile.open(output_FileName);
    std::cout.precision(10);

    std::string category;
    std::string tempStr;

    // **********   screws     ************* //
    category = "screw";
    writeFile.write(category.c_str(), category.size());
    writeFile << std::endl;
    for (auto screw : m_screws)
    {
        writeFile << std::setprecision(10) << screw[0] << ", " << screw[1] << ", " << screw[2] << ", " <<
                                 screw[3] << ", " << screw[4] << ", " << screw[5] << std::endl;
    }
    writeFile << std::endl;

    // **********   m_M_links     ************* //
    category = "M_links";
    writeFile.write(category.c_str(), category.size());
    writeFile << std::endl;

    int i = 0;
    for (auto M : m_M_links)
    {
        writeFile << i << std::endl;
        for ( int i = 0; i < 4; i++ )
        {
            for ( int j = 0; j < 4; j++ )
            {
                if ( M(i,j) >= 0.0 ) writeFile << std::setprecision(10) << " " << setw(12) << M(i,j) << " ";
                else writeFile << setw(13) << M(i,j) << " ";
            }
            writeFile << ";" << endl;
        }
        ++i;
    }
    writeFile << std::endl;

    // **********   inertias     ************* //
    category = "inertia";
    writeFile.write(category.c_str(), category.size());
    writeFile << std::endl;

    i = 0;
    for (auto inertia : m_link_inertia)
    {
        writeFile << i << std::endl;
        writeFile << std::setprecision(10) << inertia[0] << ", " << inertia[1] << ", " << inertia[2] << ", " <<
                                   inertia[3] << ", " << inertia[4] << ", " << inertia[5] << ", " <<
                                   inertia[6] << ", " << inertia[7] << ", " << inertia[8] << ", " <<
                                   inertia[9] << std::endl;
        ++i;
    }
    writeFile << std::endl;


    // **********   is Joint?     ************* //
    category = "is_joint";
    writeFile.write(category.c_str(), category.size());
    writeFile << std::endl;

    for (auto isJoint : m_is_joint)
    {
        writeFile << isJoint << ", " << std::endl;
    }
    writeFile << std::endl;


    // **********   other frames     ************* //
    category = "frames";
    writeFile.write(category.c_str(), category.size());
    writeFile << std::endl;
    tempStr = "base";
    writeFile.write(tempStr.c_str(), tempStr.size());
    writeFile << std::endl;
    for ( int i = 0; i < 4; i++ )
    {
        for ( int j = 0; j < 4; j++ )
        {
            if ( m_T_base(i,j) >= 0.0 ) writeFile << std::setprecision(10) << " " << setw(12) << m_T_base(i,j) << " ";
            else writeFile << setw(13) << m_T_base(i,j) << " ";
        }
        writeFile << ";" << endl;
    }

    tempStr = "tcp";
    writeFile.write(tempStr.c_str(), tempStr.size());
    writeFile << std::endl;
    SE3 M_target = m_M_tool;
    for ( int i = 0; i < 4; i++ )
    {
        for ( int j = 0; j < 4; j++ )
        {
            if ( M_target(i,j) >= 0.0 ) writeFile << std::setprecision(10) << " " << setw(12) << M_target(i,j) << " ";
            else writeFile << setw(13) << M_target(i,j) << " ";
        }
        writeFile << ";" << endl;
    }

    tempStr = "ft sensor";
    writeFile.write(tempStr.c_str(), tempStr.size());
    writeFile << std::endl;
    M_target = m_M_FTSensor;
    for ( int i = 0; i < 4; i++ )
    {
        for ( int j = 0; j < 4; j++ )
        {
            if ( M_target(i,j) >= 0.0 ) writeFile << std::setprecision(10) << " " << setw(12) << M_target(i,j) << " ";
            else writeFile << setw(13) << M_target(i,j) << " ";
        }
        writeFile << ";" << endl;
    }


    // **********   joint limits     ************* //
    tempStr = "joint limits";
    writeFile.write(tempStr.c_str(), tempStr.size());
    writeFile << std::endl;

    tempStr = "upper limits";
    writeFile.write(tempStr.c_str(), tempStr.size());
    writeFile << std::endl;

    for (int i = 0; i < m_dof; ++i)
    {
        writeFile << std::setprecision(10) << (*m_theta_limit_upper)[i] << ", ";
    }
    writeFile << std::endl;

    tempStr = "lower limits";
    writeFile.write(tempStr.c_str(), tempStr.size());
    writeFile << std::endl;

    for (int i = 0; i < m_dof; ++i)
    {
        writeFile << std::setprecision(10) << (*m_theta_limit_lower)[i] << ", ";
    }
    writeFile << std::endl;

    tempStr = "velocity upper limits";
    writeFile.write(tempStr.c_str(), tempStr.size());
    writeFile << std::endl;

    for (int i = 0; i < m_dof; ++i)
    {
        writeFile << std::setprecision(10) << (*m_theta_dot_limit_upper)[i] << ", ";
    }
    writeFile << std::endl;


    writeFile.close();
}

unsigned int generalRobot::getDof()
{
	return m_dof;
}

unsigned int generalRobot::getNumConnect()
{
	return m_num_connect;
}

double generalRobot::getReductionRatio()
{
	return m_reducRatio;
}

SE3 generalRobot::getBaseFrame()
{
	return m_T_base;
}

SE3 generalRobot::getLinkFrame(unsigned int _i)
{
	return m_T_links[_i];
}

SE3 generalRobot::getToolFrame()
{
    return m_T_tool;
}

SE3 generalRobot::getFTSensorFrame()
{
    return m_M_FTSensor;
}

std::vector<SE3> generalRobot::getLinkTransform()
{
    return m_M_links;
}

SE3 generalRobot::getTCPTransform()
{
    return m_M_tool;
}


Eigen::VectorXd generalRobot::getUpperLimits()
{
	return *m_theta_limit_upper;
}
Eigen::VectorXd generalRobot::getLowerLimits()
{
	return *m_theta_limit_lower;
}

bool generalRobot::checkJointLimit()
{
	for (int i = 0; i < m_dof; ++i)
		if ((*m_theta)(i) >(*m_theta_limit_upper)(i) || (*m_theta)(i) < (*m_theta_limit_lower)(i))
			return false;

	return true;
}


Eigen::VectorXd* generalRobot::getThetaPointer()
{
	return m_theta;
}

Eigen::VectorXd* generalRobot::getThetaDotPointer()
{
	return m_theta_dot;
}

SO3 generalRobot::FindOrientation(const char* _parent1, const char* _parent2, const char* _child1, const char* _child2)
{
	Axis x = FindAxis(_parent1);
	Axis y = FindAxis(_parent2);

	SO3 returnOri(x,y,Cross(x,y));

	x = FindAxis(_child1);
	y = FindAxis(_child2);

	return returnOri*Inv(SO3(x,y,Cross(x,y)));
}

bool generalRobot::forwardKinematics(Eigen::VectorXd* _theta)
{
    Eigen::VectorXd currTheta;
    if (_theta == nullptr)
        currTheta = *m_theta;
    else
        currTheta = *_theta;

    bool joint_limit_flag = checkJointLimit();

	SE3 T = m_T_base;

	int j = 0;

	for (int i = 0; i < m_num_connect; ++i)
	{
        T = T*m_M_links.at(i)/*[i]*/;
        if (m_is_joint.at(i)/*[i]*/ == true)
		{
            for (int k = 0; k < m_dof_connection.at(i)/*[i]*/; ++k)
			{
                T = T*Exp(m_screws.at(i)/*[j]*/, currTheta(j));
				++j;
			}
		}
		m_T_links[i] = T;
	}
	m_T_tool = T*m_M_tool;

	for (int i = 0; i < m_num_cylder; ++i)
		m_cylder_frame[i] = m_T_links[m_cylder_prnt_jnt[i]] * m_cylder_frame_init[i];

    return joint_limit_flag;
}

Eigen::VectorXd generalRobot::inverseKinematics(SE3 _T_target, Eigen::VectorXd* _theta)
{
	sr_real eps_w = 0.0001, eps_v = 0.0001;

	Eigen::VectorXd currTheta;

	if (_theta == NULL)
		currTheta = *m_theta;
	else
		currTheta = *_theta;

	Eigen::MatrixXd toolJaco,Rtest,QRtest;
	Eigen::VectorXd result;

	se3 diff = Log(Inv(m_T_tool)*_T_target);

	Axis w = diff.ori(); Vec3 v = diff.pos();
	Axis w_prev(100); Vec3 v_prev(100);
	int i = 0; 

	while (Norm(w)>eps_w || Norm(v)>eps_v)
	{
		++i;

		if (i > 100)
		{
			cout << "Connot find the inverse kinematic solutions..." << endl;

			return *m_theta;
		}

		toolJaco = getToolJacobian();

		while(toolJaco.colPivHouseholderQr().rank() < m_dof)
		{
			currTheta = currTheta + 0.5*Eigen::VectorXd::Random(m_dof);
			forwardKinematics(&currTheta);
			toolJaco = getToolJacobian();
		}

		currTheta = currTheta + toolJaco.colPivHouseholderQr().solve(se3ToVectorXd(diff));
		forwardKinematics(&currTheta);
		diff = Log(Inv(m_T_tool)*_T_target);
		w_prev = w; v_prev = v;
		w = diff.ori(), v = diff.pos();
	}

	resolveAngle(&currTheta);

	cout << Norm(w) <<" "<<Norm(w) << endl;

	cout << i << " iteration."<<endl;

	return currTheta;
}

vector<se3> generalRobot::getScrews()
{
	return m_screws;
}

vector<Inertia> generalRobot::getInertias()
{

	return m_link_inertia;
}

Eigen::MatrixXd generalRobot::getSpaceJacobian()
{
	Eigen::MatrixXd sJacobian(6, m_dof);
	se3 temp;
	int	j = 0;

	for (int i = 0; i < m_num_connect; ++i)
	{
		if (m_is_joint[i] == true)
		{
			temp = Ad(m_T_links[i], m_screws[j]);
			sJacobian.col(j) = se3ToVectorXd(temp);
			++j;
		}
	}

	return sJacobian;
}

Eigen::MatrixXd generalRobot::getToolJacobian()
{
	Eigen::MatrixXd tJacobian = Ad(Inv(m_T_tool))*getSpaceJacobian();

	return tJacobian;
}

Eigen::MatrixXd generalRobot::getAnalJacobian()
{
	Eigen::MatrixXd bJacobian = getToolJacobian();

//	Vec3 r = Log(m_T_tool.GetOrientation());

	
	return bJacobian;
}


Eigen::MatrixXd generalRobot::getBodyJacobianAtTc(SE3 _T_c, int _idx)
{
	int joint_idx = m_cylder_prnt_jnt[_idx];

	Eigen::MatrixXd Jacobian(6, joint_idx+1);

	SE3 tmpSE3;
	se3 temp;
	int	j = 0;

	for (int i = 0; i < joint_idx+1; ++i)
	{
		if (m_is_joint[i] == true)
		{
			tmpSE3 = Inv(_T_c)*m_T_links[i];
			temp = Ad(tmpSE3, m_screws[j]);
			Jacobian.col(j) = se3ToVectorXd(temp);
			++j;
		}
	}

	return Jacobian;
}

//Eigen::MatrixXd* generalRobot::getDerivativeToolJacobian()
//{
//	Eigen::MatrixXd bJacobian = getToolJacobian();
//	Eigen::MatrixXd *devJacobian = new Eigen::MatrixXd[m_dof];
//
//	for (int k = 0; k < m_dof; ++k)
//	{
//		devJacobian[k].setZero(6, m_dof);
//		
//		for (int i = 0; i < k; ++i)
//			devJacobian[k].col(i) = ad(bJacobian.col(i), bJacobian.col(k));
//	}
//
//	return devJacobian;
//}

void generalRobot::resolveAngle(Eigen::VectorXd* _angle)
{

	for (int i = 0; i < _angle->size();++i)
	{
		while ((*_angle)(i) > SR_PI || (*_angle)(i) < -SR_PI)
		{
			if ((*_angle)(i) > SR_PI)
				(*_angle)(i) -= 2*SR_PI;
			else
				(*_angle)(i) += 2*SR_PI;
		}
	}
}


vector<bool> generalRobot::getConnectionStatus()
{
	return m_is_joint;
}

void generalRobot::setMassMatrix()
{
	Eigen::VectorXd dth; dth.setZero(m_dof);
	Eigen::VectorXd ddth; ddth.setZero(m_dof);
	Eigen::VectorXd gravity = inverseDynamics(&ddth, NULL, &dth);

	m_mass_matrix = Eigen::MatrixXd(m_dof, m_dof);
	
	for (int i = 0; i < m_dof; ++i)
	{
		ddth.setZero();
		ddth[i] = 1.0;

		m_mass_matrix.col(i) = inverseDynamics(&ddth, NULL, &dth) - gravity;
	}
}

Eigen::MatrixXd generalRobot::getMassMatrix()
{
	setMassMatrix();

	return m_mass_matrix;

}

Eigen::VectorXd generalRobot::inverseDynamics(Eigen::VectorXd* _theta_ddot, Eigen::VectorXd* _theta, Eigen::VectorXd* _theta_dot)
{
	Eigen::VectorXd currTheta;
	Eigen::VectorXd currThetaDot;
	Eigen::VectorXd currThetaDDot;

	if (_theta == NULL)
		currTheta = *m_theta;
	else
		currTheta = *_theta;

	if (_theta_dot == NULL)
		currThetaDot = *m_theta_dot;
	else
		currThetaDot = *_theta_dot;

	if (_theta_ddot == NULL)
		currThetaDDot.setZero(m_dof);
	else
		currThetaDDot = *_theta_ddot;


	Eigen::MatrixXd  V(6, m_num_connect + 1);
	Eigen::MatrixXd dV(6, m_num_connect + 1);
	Eigen::MatrixXd  F(6, m_num_connect + 1);
	Eigen::VectorXd tau(m_dof);

	V.col(0) = m_V0;
	dV.col(0) = m_dV0;

	SE3 tmp_SE3;

	forwardKinematics(&currTheta);

	int j = 0;
	for (int i = 0; i < m_num_connect; ++i)
	{
		if (i == 0)
		{
			tmp_SE3 = Inv(m_T_links[i])*m_T_base;
		}
		else
		{
			tmp_SE3 = Inv(m_T_links[i])*m_T_links[i - 1];
		}

		V.col(i + 1) = Ad(tmp_SE3, V.col(i));
		dV.col(i + 1) = Ad(tmp_SE3, dV.col(i));
		if (m_is_joint[i] == true)
		{
			V.col(i + 1) = V.col(i + 1) + se3ToVectorXd(m_screws[j] * currThetaDot[j]);
			dV.col(i + 1) = dV.col(i + 1) + se3ToVectorXd(m_screws[j] * currThetaDDot[j]) + ad(V.col(i + 1), se3ToVectorXd(m_screws[j] * currThetaDot[j]));
			++j;
		}
	}

	j = 1;
	for (int i = m_num_connect - 1; i >= 0; --i)
	{

		if (i == m_num_connect - 1)
		{
			F.col(i + 1).setZero();
			tmp_SE3.SetEye();
		}
		else
		{
			tmp_SE3 = Inv(m_T_links[i + 1])*m_T_links[i];
		}
		F.col(i) = m_link_inertia[i] * dV.col(i + 1) - dad(V.col(i + 1), m_link_inertia[i] * V.col(i + 1)) + dAd(tmp_SE3, F.col(i + 1));

		if (m_is_joint[i] == true)
		{
			tau[m_dof-j] = m_screws[m_dof - j] * F.col(i);
			++j;
		}
	}
	return tau;
}

Eigen::VectorXd generalRobot::forwardDynamics(Eigen::VectorXd* _tau, Eigen::VectorXd* _theta, Eigen::VectorXd* _theta_dot)
{
	Eigen::VectorXd currTheta;
	Eigen::VectorXd currThetaDot;
	if (_theta == NULL)	
	{
		currTheta = *m_theta;
	}
	else
	{
		currTheta = *_theta;
	}

	if (_theta_dot == NULL)
	{
		currThetaDot = *m_theta_dot;
	}
	else
	{
		currThetaDot = *_theta_dot;
	}

	Eigen::VectorXd thetaDDot; thetaDDot.setZero(m_dof);

	Eigen::VectorXd h = inverseDynamics(&currTheta, &currThetaDot, &thetaDDot);
	thetaDDot = getMassMatrix().inverse()*(*_tau - h);

	return thetaDDot;

}

void generalRobot::addScrew(const se3 &_screw)
{
    m_screws.push_back(_screw);
}

void generalRobot::setScrews(const std::vector<se3> &_screws)
{
    for (std::vector<se3>::const_iterator iter = _screws.begin(); iter != _screws.end(); ++iter)
    {
        addScrew(*iter);
    }
}

void generalRobot::addMLink(const SE3 &_M)
{
    m_M_links.push_back(_M);
}

void generalRobot::addMLink(const SO3 &_R, const Vec3 &_p)
{
    m_M_links.push_back(SE3(_R, _p));
}

void generalRobot::setMLinks(const std::vector<SE3> &_Ms)
{
    for (std::vector<SE3>::const_iterator iter=_Ms.begin(); iter != _Ms.end(); ++iter)
    {
        addMLink(*iter);
    }
}

void generalRobot::addInertia(const double &_m, const std::vector<double> &_p, const std::vector<double> &_inertia)
{
    // Ixx, Iyy, Izz, Ixy, Ixz, Iyz
    Inertia tempInertia = Inertia(_inertia[0], _inertia[1], _inertia[2], _inertia[3], _inertia[4], _inertia[5]);
    tempInertia.SetMass(_m);
//    std::cout << tempInertia.ToArray() << std::endl;

    SE3 tempSE3 = SE3(SO3(), -Vec3(_p[0], _p[1], _p[2]));   // convert to T_com_b
    tempInertia = tempInertia.Transform(tempSE3);   // I_b = Transform(T_ab)
    m_link_inertia.push_back(tempInertia);
}

void generalRobot::addInertia(const generalRobot::InertiaTriple &_inertiaTriple)
{
    addInertia(_inertiaTriple.m, _inertiaTriple.r, _inertiaTriple.I);
}

void generalRobot::setInertias(const std::vector<generalRobot::InertiaTriple> &_inertiaTripleSet)
{
    for (std::vector<generalRobot::InertiaTriple>::const_iterator iter = _inertiaTripleSet.begin();
            iter != _inertiaTripleSet.end(); ++iter)
    {
        addInertia(*iter);
    }
}

void generalRobot::setUpperLimits(const std::vector<double> &_upperLimits)
{
    // this function should be called after calling initializeRobot
    if ((std::size_t)m_dof != _upperLimits.size())
    {
        std::cout << "WARNING: setUpperLimits: DOFs are differ." << std::endl;
        std::cout << "m_dof: " << m_dof << ", size: " << _upperLimits.size() << std::endl;
    }
    else
    {
        *m_theta_limit_upper = Eigen::VectorXd::Map(_upperLimits.data(), _upperLimits.size());
    }
}

void generalRobot::setLowerLimits(const std::vector<double> &_lowerLimits)
{
    // this function should be called after calling initializeRobot
    if ((std::size_t)m_dof != _lowerLimits.size())
    {
        std::cout << "WARNING: setLowerLimits: DOFs are differ." << std::endl;
        std::cout << "m_dof: " << m_dof << ", size: " << _lowerLimits.size() << std::endl;
    }
    else
    {
        *m_theta_limit_lower = Eigen::VectorXd::Map(_lowerLimits.data(), _lowerLimits.size());
    }
}

void generalRobot::setVelocityUpperLimits(const std::vector<double> &_upperLimits)
{
    // this function should be called after calling initializeRobot
    if ((std::size_t)m_dof != _upperLimits.size())
    {
        std::cout << "WARNING: setUpperLimits: DOFs are differ." << std::endl;
        std::cout << "m_dof: " << m_dof << ", size: " << _upperLimits.size() << std::endl;
    }
    else
    {
        *m_theta_dot_limit_upper = Eigen::VectorXd::Map(_upperLimits.data(), _upperLimits.size());
    }
}

void generalRobot::setJointProperties(const std::vector<bool> &_isJoint)
{
    m_is_activeSlave.push_back(false);      // for base

    for (auto iter : _isJoint)
    {
        m_is_joint.push_back(iter);
        if (iter == true)
        {
            m_dof_connection.push_back(1);
            m_is_activeSlave.push_back(true);
        }
        else
        {
            m_dof_connection.push_back(0);
            m_is_activeSlave.push_back(false);
        }
    }

}

void generalRobot::addTool(const SE3 &_M_Tool, const generalRobot::InertiaTriple &_Tool_Inertia)
{
    m_is_joint.push_back(false);
    m_dof_connection.push_back(0);
    m_is_activeSlave.push_back(false);

    this->addMLink(_M_Tool);
    this->addInertia(_Tool_Inertia);

}

void generalRobot::addTCP(const SE3 &_tcpFrame)
{
    m_M_tool = _tcpFrame;
}

void generalRobot::setFTSensor(const SE3 &_ftsensor)
{
    m_M_FTSensor = _ftsensor;
}

void generalRobot::initializeRobot()
{
    // after calling initializeRobot,
    // need to sync joint angles.

    // KINEMATICS
    // setScrews
    // setMLinks
    // TODO: set tool's link frame.

    // DYNAMICS
    // setInertias

    m_dof = m_screws.size();
    m_num_connect = m_M_links.size();

    // TODO: sync initial joint angles with m_iniTheta
    m_initTheta = new Eigen::VectorXd(m_dof);
    m_initTheta->setZero(m_dof);

    m_theta_limit_upper = new Eigen::VectorXd(m_dof);
    m_theta_limit_lower = new Eigen::VectorXd(m_dof);
    m_theta_dot_limit_upper = new Eigen::VectorXd(m_dof);

    for (int i = 0; i < m_dof; ++i)
    {
        (*m_theta_limit_lower)(i, 0) = -SR_PI;
        (*m_theta_limit_upper)(i, 0) = SR_PI;
        (*m_theta_dot_limit_upper)(i, 0) = SR_PI;
    }

    m_reducRatio = 1;

    // current information
    m_T_links = new SE3[m_num_connect];
    m_theta = new Eigen::VectorXd(m_dof);
    m_theta_dot = new Eigen::VectorXd(m_dof, 1);

    *m_theta = *m_initTheta;
    m_theta_dot->setZero();

    m_num_cylder = 0;

    forwardKinematics(m_theta);

}



Eigen::VectorXd generalRobot::getInitTheta()
{
	return *m_initTheta;
}


Eigen::MatrixXd generalRobot::getBodyJacobianAtCP(int _idx_cyl, int _idx_CP)
{
	int joint_idx = m_cylder_prnt_jnt[_idx_cyl];

	SE3 Cylder_center = m_cylder_frame[_idx_cyl];

	SE3 realPose = Cylder_center; realPose.SetPosition(Cylder_center.GetPosition() + m_controlPoints_pos[_idx_cyl][_idx_CP] * Cylder_center.GetOrientation().GetY());

	Eigen::MatrixXd Jacobian(6, joint_idx + 1);

	SE3 tmpSE3;
	int	j = 0;

	for (int i = 0; i < joint_idx + 1; ++i)
	{
		if (m_is_joint[i] == true)
		{
			tmpSE3 = Inv(realPose)*m_T_links[i];
			Jacobian.col(j) = se3ToVectorXd(Ad(tmpSE3, m_screws[j]));
			++j;
		}
	}

	return Jacobian;
}



Eigen::MatrixXd* generalRobot::getA()
{
	Eigen::MatrixXd *A = new Eigen::MatrixXd[m_dof];

	SE3 tmpSE3;


	for (int k = 0; k < m_dof; ++k)
	{
		A[k].setZero(6, k + 1);
		A[k].col(k) = se3ToVectorXd(m_screws[k]);

		for (int i = 0; i < k; ++i)
		{
			tmpSE3 = Inv(m_T_links[k])*m_T_links[i];
			A[k].col(i) = se3ToVectorXd(Ad(tmpSE3, m_screws[i]));
		}
	}

	return A;
}

Eigen::MatrixXd generalRobot::getMassfromA(Eigen::MatrixXd* _A)
{
	Eigen::MatrixXd rtnMass; rtnMass.setZero(m_dof, m_dof);

	for (int i = 0; i < m_dof; ++i)
	{
		for (int j = 0; j < m_dof; ++j)
		{
			for (int k = max(i, j); k < m_dof; ++k)
			{
				rtnMass(i, j) = rtnMass(i, j) + (_A[k].col(i).transpose()*(m_link_inertia[k] * _A[k].col(j)))[0];
			}
		}
	}

	return rtnMass;
}


Eigen::MatrixXd generalRobot::getMassDer(Eigen::MatrixXd* _A, int _k)
{
	Eigen::MatrixXd _massDer;
	_massDer.setZero(m_dof, m_dof);

	if (_k != 0)
	{
		int it_start;

		for (int i = 0; i < m_dof; ++i)
		{
			for (int j = 0; j < _k; ++j)
			{
				it_start = max(i, _k);

				for (int _it = it_start; _it < m_dof; ++_it)
				{
					_massDer(i, j) = _massDer(i, j) + (_A[_it].col(j).transpose()*(m_link_inertia[_it] * m_dA[_k - 1].col((m_dof - _k)*j + _it - _k)))[0];
				}
			}
		}
	}

	return _massDer;
}


Eigen::MatrixXd* generalRobot::getdA(Eigen::MatrixXd* _A)
{
	int _iter;
	Eigen::MatrixXd *dA = new Eigen::MatrixXd[m_dof - 1];
	for (int k = 1; k < m_dof; ++k)
	{
		_iter = 0;
		dA[k - 1].setZero(6, (m_dof - k)*k);

		for (int j = 0; j < k; ++j)
		{
			for (int i = k; i < m_dof; ++i)
			{
				dA[k - 1].col(_iter) = ad(_A[i].col(j), _A[i].col(k));
				++_iter;
			}
		}
	}

	return dA;
}

void generalRobot::setdA(Eigen::MatrixXd* _dA)
{
	m_dA = _dA;
}


//////////////////////////// Eigen functions and operators//////////////////


Eigen::VectorXd se3ToVectorXd(const se3& _se3)
{
	Eigen::VectorXd rtnVec(6);

	for (int i = 0; i < 6; ++i)
		rtnVec[i] = _se3[i];

	return rtnVec;
}

Eigen::Vector3d Vec3ToVector3d(Vec3 _vec3)
{
	Eigen::Vector3d rtnVec;

	for (int i = 0; i < 3; ++i)
		rtnVec[i] = _vec3[i];

	return rtnVec;

}


Eigen::MatrixXd Ad(SE3 _T)
{
	Eigen::MatrixXd returnMat(6, 6);
	returnMat.setZero();

	for (int i = 0; i < 3; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			returnMat(i, j) = _T(i, j);
			returnMat(i + 3, j + 3) = _T(i, j);
		}
	}

	returnMat(3, 0) = -_T(2, 3)*_T(1, 0) + _T(1, 3)*_T(2, 0);
	returnMat(3, 1) = -_T(2, 3)*_T(1, 1) + _T(1, 3)*_T(2, 1);
	returnMat(3, 2) = -_T(2, 3)*_T(1, 2) + _T(1, 3)*_T(2, 2);

	returnMat(4, 0) = -_T(0, 3)*_T(2, 0) + _T(2, 3)*_T(0, 0);
	returnMat(4, 1) = -_T(0, 3)*_T(2, 1) + _T(2, 3)*_T(0, 1);
	returnMat(4, 2) = -_T(0, 3)*_T(2, 2) + _T(2, 3)*_T(0, 2);

	returnMat(5, 0) = -_T(1, 3)*_T(0, 0) + _T(0, 3)*_T(1, 0);
	returnMat(5, 1) = -_T(1, 3)*_T(0, 1) + _T(0, 3)*_T(1, 1);
	returnMat(5, 2) = -_T(1, 3)*_T(0, 2) + _T(0, 3)*_T(1, 2);

	return returnMat;
}

Eigen::VectorXd ad(Eigen::VectorXd s1, Eigen::VectorXd s2)
{
	Eigen::VectorXd returnVec(6, 1);
	returnVec[0] = s1[1] * s2[2] - s1[2] * s2[1];
	returnVec[1] = s1[2] * s2[0] - s1[0] * s2[2];
	returnVec[2] = s1[0] * s2[1] - s1[1] * s2[0];
	returnVec[3] = s1[1] * s2[5] - s1[2] * s2[4] - s2[1] * s1[5] + s2[2] * s1[4];
	returnVec[4] = s1[2] * s2[3] - s1[0] * s2[5] - s2[2] * s1[3] + s2[0] * s1[5];
	returnVec[5] = s1[0] * s2[4] - s1[1] * s2[3] - s2[0] * s1[4] + s2[1] * s1[3];

	return returnVec;
}



Eigen::VectorXd Ad(const SE3 &T, const Eigen::VectorXd &s)
{
	sr_real tmp[3] = { T[0] * s[0] + T[3] * s[1] + T[6] * s[2],
		T[1] * s[0] + T[4] * s[1] + T[7] * s[2],
		T[2] * s[0] + T[5] * s[1] + T[8] * s[2] };

	Eigen::VectorXd rtnVec(6);

	rtnVec << tmp[0], tmp[1], tmp[2],
		T[10] * tmp[2] - T[11] * tmp[1] + T[0] * s[3] + T[3] * s[4] + T[6] * s[5],
		T[11] * tmp[0] - T[9] * tmp[2] + T[1] * s[3] + T[4] * s[4] + T[7] * s[5],
		T[9] * tmp[1] - T[10] * tmp[0] + T[2] * s[3] + T[5] * s[4] + T[8] * s[5];
	return rtnVec;
}


Eigen::VectorXd dAd(const SE3 &T, const Eigen::VectorXd &t)
{
	sr_real tmp[3] = { t[0] - T[10] * t[5] + T[11] * t[4],
		t[1] - T[11] * t[3] + T[9] * t[5],
		t[2] - T[9] * t[4] + T[10] * t[3] };
	Eigen::VectorXd rtnVec(6);
	rtnVec << T[0] * tmp[0] + T[1] * tmp[1] + T[2] * tmp[2],
		T[3] * tmp[0] + T[4] * tmp[1] + T[5] * tmp[2],
		T[6] * tmp[0] + T[7] * tmp[1] + T[8] * tmp[2],
		T[0] * t[3] + T[1] * t[4] + T[2] * t[5],
		T[3] * t[3] + T[4] * t[4] + T[5] * t[5],
		T[6] * t[3] + T[7] * t[4] + T[8] * t[5];
	return rtnVec;
}

Eigen::VectorXd dad(const Eigen::VectorXd &s, const Eigen::VectorXd &t)
{
	Eigen::VectorXd rtnVec(6);

	rtnVec << t[1] * s[2] - t[2] * s[1] + t[4] * s[5] - t[5] * s[4],
		t[2] * s[0] - t[0] * s[2] + t[5] * s[3] - t[3] * s[5],
		t[0] * s[1] - t[1] * s[0] + t[3] * s[4] - t[4] * s[3],
		t[4] * s[2] - t[5] * s[1],
		t[5] * s[0] - t[3] * s[2],
		t[3] * s[1] - t[4] * s[0];

	return rtnVec;
}


Eigen::MatrixXd operator*(SO3 _R, Axis _v)
{
	Eigen::MatrixXd rtnMat(3, 3);

	for (int i = 0; i < 3; ++i)
	{
		rtnMat(i, 0) = _R(i, 1)*_v[2] - _R(i, 2)*_v[1];
		rtnMat(i, 1) = _R(i, 2)*_v[0] - _R(i, 0)*_v[2];
		rtnMat(i, 2) = _R(i, 0)*_v[1] - _R(i, 1)*_v[0];
	}

	return rtnMat;

}

Eigen::MatrixXd operator*(SE3 _T, se3 _v)
{
	Eigen::MatrixXd rtnMat(4, 4); rtnMat.setZero();

	for (int i = 0; i < 3; ++i)
	{
		rtnMat(i, 0) = _T(i, 1)*_v[2] - _T(i, 2)*_v[1];
		rtnMat(i, 1) = _T(i, 2)*_v[0] - _T(i, 0)*_v[2];
		rtnMat(i, 2) = _T(i, 0)*_v[1] - _T(i, 1)*_v[0];

		rtnMat(i, 3) = _T(i,0)*_v[3] + _T(i, 1)*_v[4] + _T(i, 2)*_v[5] + _T(i,3);
	}

	return rtnMat;
}

Eigen::MatrixXd operator*(Eigen::MatrixXd _mat, SO3 _R)
{
	Eigen::MatrixXd rtnMat(_mat.rows(), 3);

	rtnMat.setZero();

	for (int i = 0; i < _mat.rows(); ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			for (int k = 0; k < 3; ++k)
				rtnMat(i, j) = rtnMat(i, j) + _mat(i, k)*_R(k, j);
		}
	}

	return rtnMat;
}

Eigen::MatrixXd operator*(SO3 _R, Eigen::MatrixXd _mat)
{
	Eigen::MatrixXd rtnMat(3, _mat.cols());

	rtnMat.setZero();

	for (int i = 0; i < 3; ++i)
	{
		for (int j = 0; j < _mat.cols(); ++j)
		{
			for (int k = 0; k < 3; ++k)
				rtnMat(i, j) = rtnMat(i, j) + _R(i, k)*_mat(k, j);
		}
	}

	return rtnMat;
}


Eigen::MatrixXd operator*(Eigen::MatrixXd _mat, SE3 _T)
{
	Eigen::MatrixXd rtnMat(_mat.rows(), 4);

	rtnMat.setZero();

	for (int i = 0; i < _mat.rows(); ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			for (int k = 0; k < 4; ++k)
				rtnMat(i, j) = rtnMat(i, j) + _mat(i, k)*_T(k, j);
		}
	}

	return rtnMat;
}

Eigen::MatrixXd operator*(SE3 _T, Eigen::MatrixXd _mat)
{
	Eigen::MatrixXd rtnMat(4, _mat.cols());

	rtnMat.setZero();

	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < _mat.cols(); ++j)
		{
			for (int k = 0; k < 4; ++k)
				rtnMat(i, j) = rtnMat(i, j) + _T(i, k)*_mat(k, j);
		}
	}

	return rtnMat;
}



sr_real operator*(se3 s, Eigen::VectorXd t)
{
	return (t[0] * s[0] + t[1] * s[1] + t[2] * s[2] + t[3] * s[3] + t[4] * s[4] + t[5] * s[5]);
}

Eigen::VectorXd operator*(Inertia Inert, Eigen::VectorXd s)
{
	Eigen::VectorXd rtnVec(6);

	rtnVec << Inert[0] * s[0] + Inert[3] * s[1] + Inert[4] * s[2] + Inert[7] * s[5] - Inert[8] * s[4],
		Inert[3] * s[0] + Inert[1] * s[1] + Inert[5] * s[2] + Inert[8] * s[3] - Inert[6] * s[5],
		Inert[4] * s[0] + Inert[5] * s[1] + Inert[2] * s[2] + Inert[6] * s[4] - Inert[7] * s[3],
		s[1] * Inert[8] - s[2] * Inert[7] + Inert[9] * s[3],
		s[2] * Inert[6] - s[0] * Inert[8] + Inert[9] * s[4],
		s[0] * Inert[7] - s[1] * Inert[6] + Inert[9] * s[5];

	return rtnVec;
}

Eigen::MatrixXd operator*(Eigen::MatrixXd _mat, Vec3 _v)
{
	Eigen::MatrixXd rtnMat(_mat.rows(), 1);

	rtnMat.setZero();

	for (int i = 0; i < _mat.rows(); ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			rtnMat(i, 0) = rtnMat(i, 0) + _mat(i, j)*_v[j];
		}
	}

	return rtnMat;
}

sr_real Inner(Eigen::VectorXd &p, const Vec3 &q)
{
	if (p.size() != 3)
	{
		cout << "Inner: wrong Vector dimension" << endl;
		return -1000000000000000;
	}

	return (p[0] * q[0] + p[1] * q[1] + p[2] * q[2]);
}

Eigen::VectorXd operator-(Eigen::VectorXd p, Vec3 q)
{
	Eigen::VectorXd rtnVec(3);

	if (p.size() != 3)
	{
		cout << "Inner: wrong Vector dimension" << endl;
		return rtnVec;
	}

	for (int i = 0; i < 3; ++i)
	{
		rtnVec[i] = p[i] - q[i];
	}

	return rtnVec;
}

Eigen::MatrixXd Skew(Vec3 q)
{
	Eigen::MatrixXd rtnMat(3, 3); rtnMat.setZero();

	rtnMat(1, 2) = -q[0];
	rtnMat(0, 2) = q[1];
	rtnMat(0, 1) = -q[2];

	rtnMat(2, 1) = q[0];
	rtnMat(2, 0) = -q[1];
	rtnMat(1, 0) = q[2];

	return rtnMat;
}

Eigen::MatrixXd Skew(se3 q)
{
	Eigen::MatrixXd rtnMat(4, 4); rtnMat.setZero();

	rtnMat(1, 2) = -q[0];
	rtnMat(0, 2) = q[1];
	rtnMat(0, 1) = -q[2];

	rtnMat(2, 1) = q[0];
	rtnMat(2, 0) = -q[1];
	rtnMat(1, 0) = q[2];

	rtnMat(0,3) = q[3];
	rtnMat(1,3) = q[4];
	rtnMat(2,3) = q[5];

	return rtnMat;
}

Vec3 operator+(Eigen::VectorXd v0, Vec3 v1)
{
	Vec3 rtnVec;
	for (int i = 0; i < 3; ++i)
		rtnVec[i] = v0[i] + v1[i];

	return rtnVec;
}
