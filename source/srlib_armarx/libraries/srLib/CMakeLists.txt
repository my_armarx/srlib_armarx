armarx_add_library(srLib
    SOURCES
        ./srLib.cpp
        ./generalRobot.cpp
        ./LieGroup.cpp
        ./utils.cpp
        ./srLib_armarx_converter.h
    HEADERS
        ./srLib.h
        ./generalRobot.h
        ./LieGroup.h
        ./utils.h
        ./srLib_armarx_converter.cpp
    DEPENDENCIES
        ArmarXCoreInterfaces
        ArmarXCore
)


#armarx_add_test(srLibTest
#    TEST_FILES
#        srLibTest.cpp
#    DEPENDENCIES
#        srlib_armarx::srLib
#)

