/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    srlib_armarx
 * @author     Byungchul An ( byungchul dot an at gmail dot com )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "LieGroup.h"

#include <Eigen/Dense>

#include <string>
#include <vector>
#include <map>



namespace armarx {
namespace srLib {

    /// srLib has a twist V = (w, v) representation.    se3, dse3, ...
    /// armarx has a twist V = (v, w) representation.   Eigen::Vector class
    /// This is the same as all over the context, wrench, jacobian, adjoint, ......

    // se(3)
    se3 EigenTose3(const Eigen::VectorXf& V);
    Eigen::VectorXf se3ToEigen(const se3& S);
    Eigen::Matrix4f vecTose3(const Eigen::VectorXf& V);
    Eigen::VectorXf se3Tovec(const Eigen::Matrix4f& S);
    Eigen::VectorXf changese3Order(const Eigen::VectorXf& S);

    // SO(3)
    Eigen::Matrix3f skew(const Eigen::Vector3f& w);

    // SE(3)
    SE3 EigenToSE3(const Eigen::Matrix4f& _in);
    Eigen::Matrix4f SE3ToEigen(const SE3& _in);
    Eigen::Matrix4f InverseSE3(const Eigen::Matrix4f& _in);

    Eigen::VectorXf LogSE3(const Eigen::Matrix4f& T);
    //    Eigen::Matrix4f ExpSE3(const Eigen::MatrixXf& T);
    Eigen::Matrix4f ExpSE3(const Eigen::VectorXf& V);

    Eigen::MatrixXf Ad(const Eigen::Matrix4f& T);
    Eigen::MatrixXf dAd(const Eigen::Matrix4f& T);

    Eigen::VectorXf ad(const Eigen::VectorXf& V1, const Eigen::VectorXf& V2);

}

}
